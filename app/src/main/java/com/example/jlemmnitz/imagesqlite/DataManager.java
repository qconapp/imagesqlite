package com.example.jlemmnitz.imagesqlite; /**
 * Created by jlemmnitz on 31.03.2015.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;

import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.ByteArrayOutputStream;

public class DataManager {

    // diese Klasse verwaltet die Zugriff auf die Datenbank
    // bekommt die Daten der GUI und senden eine Aufbereitung zurück!

    // globale Variablen
    ArrayList<HashMap<String, String>> alTemplateImageList = new ArrayList<HashMap<String, String>>();

    // private globlae Klasse
    private DBAccess imageDBAccess;

    // Konstruktor mit Context
    public DataManager(Context context)
    {
        imageDBAccess = new DBAccess(context);
    }

    public ArrayList<String> getTemplateImageList()
    {
        // Das Sammelobjekt fuer das Ergebnis
        ArrayList<HashMap<String, String>> alTemplateImageList = new ArrayList<HashMap<String, String>>();

        try {
            // Auslesen der ID und des Names der Template Images
            SQLiteDatabase db = imageDBAccess.getReadableDatabase();
            String strSelectQuery = "SELECT " +
                    ImageDBModel.TABLE_Temp_COLUMN_id + ", " +
                    ImageDBModel.TABLE_Temp_COLUMN_name +

                    ", " + ImageDBModel.TABLE_Temp_COLUMN_deleted +

                    " FROM " + ImageDBModel.TABLE_Temp +
                    " WHERE " + ImageDBModel.TABLE_Temp_COLUMN_deleted + " IS NOT ?";
            System.err.println("DEBUG: DataManager: getTemplateImageIDByName: strSelectQuery = " + strSelectQuery.toString());
            Cursor curSelectQuery = db.rawQuery(strSelectQuery, new String[] {"1"} );
            //Cursor curSelectQuery = db.rawQuery(strSelectQuery, null);

            if (curSelectQuery.moveToFirst()) {
                // wenn es einen erste Eintrag gibt so geht die Anwendung hier her!
                do {
                    HashMap<String, String> actTempImageDate = new HashMap<String, String>();
                    actTempImageDate.put(ImageDBModel.TABLE_Temp_COLUMN_id, curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Temp_COLUMN_id)));
                    actTempImageDate.put(ImageDBModel.TABLE_Temp_COLUMN_name, curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Temp_COLUMN_name)));

                    System.err.println("DEBUG: INFO "  + curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Temp_COLUMN_id)) + " - " +
                            curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Temp_COLUMN_name)) + " - " +
                            curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Temp_COLUMN_deleted)) );

                    alTemplateImageList.add(actTempImageDate);
                } while (curSelectQuery.moveToNext());
            }
            db.close();
        }
        catch (Exception strErr)
        {
            System.err.println("ERROR: DataManager: getTemplateImageList: "  + strErr.toString());
        }

        System.err.println("DEBUG DataManager: alTemplateImageList: " + alTemplateImageList.toString());
        ArrayList<String> alTemplateImageName = new ArrayList<String>();
        if (alTemplateImageList.isEmpty()) {
            System.err.println("WARNING: DataManager: alTemplateImageList ist LEER!");
        }
        else
        {
            try {
                // Liste aus dem DB Access Module laden und speichern
                for (HashMap<String, String> loopHashMap : alTemplateImageList) {
                    // die HashMap durchlaufen
                    // der aktuelle Hash Eintag hab zwei Keys!
                    String strHash_id = null;
                    String strHash_name = null;
                    for (String actKey : loopHashMap.keySet()) {   // lesen key t_id und Namen t_name aus der Liste
                        if (actKey == ImageDBModel.TABLE_Temp_COLUMN_id) {strHash_id = loopHashMap.get(actKey);}
                        else if (actKey == ImageDBModel.TABLE_Temp_COLUMN_name) {strHash_name = loopHashMap.get(actKey);}
                    }
                    // wenn wirklich etwas gefunden wruden dann wird das hier ausgegeben
                    if (strHash_id != null && strHash_name != null) {
                        alTemplateImageName.add(strHash_id + ":" + strHash_name);
                    }
                }
            } catch (Exception strErr) {
                System.err.println("ERROR DataManager: getTemplateImageList: " + strErr.toString());
            }
        }
        // Liste zurückgeben
        System.err.println("DEBUG DataManager: getTemplateImageList: " + alTemplateImageName.toString());
        return alTemplateImageName;
    }

    public ArrayList<String> getMatchingResultsList(Boolean boolMatched)
    {
        // Auslesen der Matching Results
        ArrayList<HashMap<String, String>> alHaMatchingResultsList = new ArrayList<HashMap<String, String>>();

        try {
            // setzte matched Flag auf 0 false oder 1 true
            String strMatched = "0";
            if (boolMatched == true) {
                strMatched = "1";
            }

            // Auslesen der ID und des Names der Template Images
            SQLiteDatabase db = imageDBAccess.getReadableDatabase();
            String strSelectQuery = "SELECT " +
                    "MaRe." + ImageDBModel.TABLE_MaRe_COLUMN_id + ", " +
                    "Pics." + ImageDBModel.TABLE_Pics_COLUMN_name + ", " +
                    "Temp." + ImageDBModel.TABLE_Temp_COLUMN_name + ", " +
                    "MaRe." + ImageDBModel.TABLE_MaRe_COLUMN_matched +
                    " FROM " +
                        ImageDBModel.TABLE_MaRe + " MaRe, " +
                        ImageDBModel.TABLE_Pics + " Pics, " +
                        ImageDBModel.TABLE_Temp + " Temp " +
                    " WHERE MaRe." + ImageDBModel.TABLE_MaRe_COLUMN_matched + " IS " + strMatched + " " +
                    //"   AND MaRe." + ImageDBModel.TABLE_MaRe_COLUMN_deleted + " IS NOT ?" +
                    "   AND MaRe." + ImageDBModel.TABLE_MaRe_COLUMN_p_id + " = Pics." + ImageDBModel.TABLE_Pics_COLUMN_id +
                    "   AND MaRe." + ImageDBModel.TABLE_MaRe_COLUMN_t_id + " = Temp."  +ImageDBModel.TABLE_Temp_COLUMN_id;
            System.err.println("DEBUG: DataManager: getMatchingResultsList: strSelectQuery = " + strSelectQuery.toString()  +" --boolMatched:"  +boolMatched.toString());
            // Cursor curSelectQuery = db.rawQuery(strSelectQuery, new String[] {strMatched.toString(), "1".toString())} );
            Cursor curSelectQuery = db.rawQuery(strSelectQuery, null);

            if (curSelectQuery != null && curSelectQuery.moveToFirst()) {
                // wenn es einen erste Eintrag gibt so geht die Anwendung hier her!
                do {
                    HashMap<String, String> actMaReData = new HashMap<String, String>();
                    actMaReData.put(ImageDBModel.TABLE_MaRe_COLUMN_id, curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_MaRe_COLUMN_id)));
                    actMaReData.put(ImageDBModel.TABLE_Pics_COLUMN_name, curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Pics_COLUMN_name)));
                    actMaReData.put(ImageDBModel.TABLE_Temp_COLUMN_name, curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Temp_COLUMN_name)));

                    System.err.println("DEBUG: INFO "  + curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_MaRe_COLUMN_id)) + " - " +
                            curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Pics_COLUMN_name)) + " - " +
                            curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Temp_COLUMN_name)) + " - " +
                            curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_MaRe_COLUMN_matched)) );

                    alHaMatchingResultsList.add(actMaReData);
                } while (curSelectQuery.moveToNext());
            }
            db.close();
        }
        catch (Exception strErr)
        {
            System.err.println("ERROR: DataManager: getMatchingResultsList: "  + strErr.toString());
        }

        System.err.println("DEBUG DataManager: getMatchingResultsList: " + alHaMatchingResultsList.toString());
        ArrayList<String> alMatchingResultsList = new ArrayList<String>();
        if (alHaMatchingResultsList.isEmpty()) {
            System.err.println("WARNING: DataManager: getMatchingResultsList ist LEER!");
        }
        else
        {
            try {
                // Liste aus dem DB Access Module laden und speichern
                for (HashMap<String, String> loopHashMap : alHaMatchingResultsList) {
                    // die HashMap durchlaufen
                    // der aktuelle Hash Eintag hab zwei Keys!
                    String strHash_id = null;
                    String strHash_p_name = null;
                    String strHash_t_name = null;

                    for (String actKey : loopHashMap.keySet()) {   // lesen key t_id und Namen t_name aus der Liste
                        if (actKey == ImageDBModel.TABLE_MaRe_COLUMN_id) {strHash_id = loopHashMap.get(actKey);}
                        else if (actKey == ImageDBModel.TABLE_Pics_COLUMN_name) {strHash_p_name = loopHashMap.get(actKey);}
                        else if (actKey == ImageDBModel.TABLE_Temp_COLUMN_name) {strHash_t_name = loopHashMap.get(actKey);}
                    }
                    // wenn wirklich etwas gefunden wruden dann wird das hier ausgegeben
                    if (strHash_id != null && strHash_p_name != null && strHash_t_name != null) {
                        alMatchingResultsList.add(strHash_id + ":" + strHash_p_name + ":" + strHash_t_name);
                    }
                }
            } catch (Exception strErr) {
                System.err.println("ERROR DataManager: getMatchingResultsList: " + strErr.toString());
            }
        }
        // Liste zurückgeben
        System.err.println("DEBUG DataManager: getMatchingResultsList: " + alMatchingResultsList.toString());
        return alMatchingResultsList;
    }

    public ArrayList<String> getTemplateImagePathNameByID(String strTempID) {
        // Suche die ID aus der Namesliste
        ArrayList<String> alRes = new ArrayList<String>();
        if (strTempID != null && strTempID != "") {
            System.err.println("DEBUG DataManager: getTemplateImagePathNameByID: ToDo: getTemplateImageDataByIDFromDB ");

            // Die Daten zu einem Template
            ArrayList<String> alPathName = new ArrayList<String>();
            try {
                SQLiteDatabase db = imageDBAccess.getReadableDatabase();
                String strSelectQuery = "SELECT " +
                        ImageDBModel.TABLE_Temp_COLUMN_name + ", " +
                        ImageDBModel.TABLE_Temp_COLUMN_path +
                        " FROM " + ImageDBModel.TABLE_Temp +
                        " WHERE " + ImageDBModel.TABLE_Temp_COLUMN_id + " = " + strTempID.toString();
                System.err.println("DEBUG: DataManager: getTemplateImageIDByName: strSelectQuery = " + strSelectQuery.toString());
                Cursor curSelectQuery = db.rawQuery(strSelectQuery, null);

                if (curSelectQuery.moveToFirst()) {
                    // wenn es einen erste Eintrag gibt so geht die Anwendung hier her!
                    do {
                        alPathName.add(curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Temp_COLUMN_path)));
                        alPathName.add(curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Temp_COLUMN_name)));
                    } while (curSelectQuery.moveToNext());
                }
                db.close();
            }
            catch (Exception strErr)
            {
                System.err.println("ERROR: DataManager: getTemplateImagePathNameByID: " + strErr.toString());
            }
            return alPathName;


        }
        return alRes;
    }


    public byte[] getTemplateImageIDByName(String strTempalteImageName)
    {
        // splitten
        String[] str_Id_Name = strTempalteImageName.split(":");
        byte[] baTempImage =  null;
        // Suche die ID aus der Namesliste
        /*
        ArrayList<String> alRes = new  ArrayList<String>();
        String strTempID = null;
        int intCountFindings = 0;
        System.err.println("DEBUG: getTemplateImageIDByName: alTemplateImageList: " + alTemplateImageList.toString());

        for (HashMap<String, String> loopHashMap : alTemplateImageList) {
            // die HashMap durchlaufen
            for (String actKey : loopHashMap.keySet())
            {   // lesen key t_id und Namen t_name aus der Liste
                // der Key mit der ID und daraus der Wert -> ID des Templates
                System.err.println("DEBUG: LOOP: actKey: " + actKey.toString() + " value: " + loopHashMap.get(actKey).toString() );
                if (actKey == ImageDBModel.TABLE_Temp_COLUMN_id && str_Id_Name[0] == loopHashMap.get(actKey))
                {
                    // Treffer mti der ID
                    strTempID = actKey;
                }
                if (actKey == ImageDBModel.TABLE_Temp_COLUMN_name && str_Id_Name[1] == loopHashMap.get(actKey))
                {
                    // Treffer auf dem Namen
                    intCountFindings++;
                }
            }
        }
        */
        /*
        if (intCountFindings > 1)
        {
            System.err.println("WARNING: more findings! " + intCountFindings);
        }
        else
        {
            // nun die Daten über die ID
            alRes = imageDBAccess.getTemplateImageByIDFromDB(strTempID);
            // Debugausgabe
            System.err.println("DEBUG: getTemplateImageIDByName: strTempID: " + strTempID);
            System.err.println("DEBUG: getTemplateImageIDByName: Result: " + alRes.toString());
        }
        */

        // nun die Daten über die ID
        byte[] baImage = null;
        if (str_Id_Name.length == 2)
        {
            System.err.println("DEBUG DataManager: getTemplateImageIDByName: ToDo: getTemplateImageByIDFromDB ");
            try {
                SQLiteDatabase db = imageDBAccess.getReadableDatabase();
                String strSelectQuery = "SELECT " +
                        ImageDBModel.TABLE_Temp_COLUMN_cont +
                        " FROM " + ImageDBModel.TABLE_Temp +
                        " WHERE " + ImageDBModel.TABLE_Temp_COLUMN_id + "=?";
                System.err.println("DEBUG: DataManager: getTemplateImageIDByName: strSelectQuery = " + strSelectQuery.toString());
                Cursor curSelectQuery = db.rawQuery(strSelectQuery, new String[] {str_Id_Name[0].toString()} );

                if (curSelectQuery.moveToFirst()) {
                    // wenn es einen erste Eintrag gibt so geht die Anwendung hier her!
                    do {
                        baImage = curSelectQuery.getBlob(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Temp_COLUMN_cont));
                    } while (curSelectQuery.moveToNext());
                }
                db.close();
            }
            catch (Exception strErr)
            {
                System.err.println("ERROR: DataManager: getTemplateImageIDByName: " + strErr.toString());
            }
            return baImage;

        }
        return baImage;
    }

    public byte[] openImageFile(String strPicPathForImageView)
    {
        byte[] byteArrayTempImage = null;
        // Lade das Bild!
        if (strPicPathForImageView == null)
        {
            return byteArrayTempImage;
        }
        else
        {
            File fImagePathName = new File(strPicPathForImageView);
            if (fImagePathName.exists())
            {
                try {
                    FileInputStream fisImage = new FileInputStream(fImagePathName);
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    byte[] buf = new byte[1024*8];

                    for (int readNum; (readNum = fisImage.read(buf)) != -1; ) {
                        bos.write(buf, 0, readNum);
                        //no doubt here is 0
                        /*Writes len bytes from the specified byte array starting at offset
                         off to this byte array output stream.*/
                        //System.err.println("NFO: DataManager: openImageFile: read " + readNum + " bytes,");
                    }
                    // das ByteArray
                    return bos.toByteArray();
                } catch (Exception strErr) {
                    System.err.println("ERROR: DataManager: openImageFile: " + strErr.toString());
                    return byteArrayTempImage;
                }
            }
            else
            {
                System.err.println("WARNING: DataManager: openImageFile: File not found: " + strPicPathForImageView.toString());
                return byteArrayTempImage;
            }
        }
    }

    public ArrayList<String> AddOrUpdateTemplateImageData(String strTempImagePath, String strTempImageName,
                   String strTempImageDesc, String strTempImageType, String strPicPathForImageView,
                   Integer intTempId, String strInfoTemp, Integer intInfoId)
    {
        // Pfad Name und das Bild zum Template.
        // laden des Bildes und speichern als byte array in der Datenbank
        if (intTempId == null) {intTempId = 0;}
        ArrayList<String> alReturn = new ArrayList<String>();

        // Lade das Image in eine ByteArray
        byte[] byteArrayTempImage = null;
        byteArrayTempImage = openImageFile(strPicPathForImageView);

        // Test nur Strings speichern: Pfad Name zum Template
        SQLiteDatabase db = imageDBAccess.getWritableDatabase();
        ContentValues cv = new ContentValues();
        try
        {
            cv.put(ImageDBModel.TABLE_Temp_COLUMN_path, strTempImagePath);
            cv.put(ImageDBModel.TABLE_Temp_COLUMN_name, strTempImageName);
            cv.put(ImageDBModel.TABLE_Temp_COLUMN_desc, strTempImageDesc);
            cv.put(ImageDBModel.TABLE_Temp_COLUMN_type, strTempImageType);
            cv.put(ImageDBModel.TABLE_Temp_COLUMN_cont, byteArrayTempImage);
            System.err.println("INFO: DataManager: AddOrUpdateTemplateImageData: length byteArrayTempImage: " + byteArrayTempImage.length);

            if (intTempId == 0)
            {
                // Speichern des Bildes
                int newTempId = (int)db.insert(ImageDBModel.TABLE_Temp, null, cv);
                if (newTempId < 1) {
                    // Speichern des Templates fehlgeschlagen!
                    db.close();
                    cv.clear();
                    System.err.println("INFO: DataManager: AddOrUpdateTemplateImageData: INSERT of Template Image failed newTempId: " + newTempId);
                    alReturn.clear();
                    alReturn.add("-9");
                    alReturn.add("INSERT of Template Image failed!");
                    return alReturn;
                    //return -9;
                }
                else
                {
                    int newInfoId = (int)this.AddOrUpdateInfoData(newTempId, strInfoTemp, intInfoId);
                    db.close();
                    cv.clear();

                    if (newInfoId < 1) {
                        // Speichern des Templates fehlgeschlagen!
                        System.err.println("INFO: DataManager: AddOrUpdateTemplateImageData: INSERT Info of Template Image failed newInfoId: " + newInfoId);
                        alReturn.clear();
                        alReturn.add("-8");
                        alReturn.add("INSERT of Template Image OK but INSERT of Info failed!");
                        return alReturn;
                        //return -9;
                    }
                    else {
                        System.err.println("INFO: DataManager: AddOrUpdateTemplateImageData: INSERT Info of Template Image done: newTempId: " + newTempId + " and Info Data: " + newInfoId);
                        alReturn.clear();
                        alReturn.add(String.valueOf(newTempId));
                        alReturn.add("INSERT of Template Image id=" + newTempId + " OK and INSERT of Info id=" + newInfoId + " OK!");
                        return alReturn;
                    }
                }
            }
            else // intTempId > 0 somit ein UPDATE
            {
                int retValueTemp = (int) db.update(ImageDBModel.TABLE_Temp, cv, ImageDBModel.TABLE_Temp_COLUMN_id + "=?", new String[]{String.valueOf(intTempId)});
                if (retValueTemp < 1) {
                    // Update des Templates fehlgeschlagen!
                    db.close();
                    cv.clear();
                    System.err.println("INFO: DataManager: AddOrUpdateTemplateImageData: UPDATE of Template Image failed intTempId: " + intTempId);
                    alReturn.clear();
                    alReturn.add("-7");
                    alReturn.add("UPDATE of Template Image id=" + intTempId + " failed!");
                    return alReturn;
                } else {   // Temp Image Update erfolgreich -> Informationseintrag anlegen / updaten
                    int retValueInfo = (int) this.AddOrUpdateInfoData(intTempId, strInfoTemp, intInfoId);
                    db.close();
                    cv.clear();

                    if (retValueInfo < 1) {
                        // Speichern des Templates fehlgeschlagen!
                        System.err.println("INFO: DataManager: AddOrUpdateTemplateImageData: UPDATE Info of Template Image failed retValueInfo: " + retValueInfo);
                        alReturn.clear();
                        alReturn.add("-6");
                        alReturn.add("UPDATE of Template Image OK but UPDATE of Info failed!");
                        return alReturn;
                        //return -9;
                    } else {
                        System.err.println("INFO: DataManager: AddOrUpdateTemplateImageData: UDPATE Info of Template Image done: id=" + intTempId + " and Info Data: id=" + retValueInfo);
                        alReturn.clear();
                        alReturn.add(String.valueOf(intTempId));
                        alReturn.add("UPDATE of Template Image  id=" + intTempId + " OK and UPDATE of Info OK!");
                        return alReturn;
                    }
                }
            }
        }
        catch(Exception strErr)
        {
            System.err.println("ERROR: DataManager: AddOrUpdateTemplateImageData: " + strErr.toString());
            db.close();
            cv.clear();
            alReturn.clear();
            alReturn.add("-4");
            alReturn.add("ERROR: AddOrUpdateTemplateImageData: " + strErr.toString());
            return alReturn;
        }
    }

    public int AddOrUpdatePictureImageData(String strPicImagePath, String strPicImageName,
                                            String strPicImageDesc, String strPicImageType, String strPicPathForImageView,
                                            Integer intPicId)
    {
        // Pfad Name und das Bild zum Pictures.
        // die Bild Daten werden NICHT in der Datenbank gespeichert!

        // Lade das Image in eine ByteArray
        byte[] byteArrayTempImage = null;
        //byteArrayTempImage = openImageFile(strPicPathForImageView);

        // Test nur Strings speichern: Pfad Name zum Template
        SQLiteDatabase db = imageDBAccess.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(ImageDBModel.TABLE_Pics_COLUMN_path, strPicImagePath);
        cv.put(ImageDBModel.TABLE_Pics_COLUMN_name, strPicImageName);
        cv.put(ImageDBModel.TABLE_Pics_COLUMN_desc, strPicImageDesc);
        cv.put(ImageDBModel.TABLE_Pics_COLUMN_type, strPicImageType);
        //cv.put(ImageDBModel.TABLE_Pics_COLUMN_cont, byteArrayTempImage);
        //System.err.println("INFO: DataManager: AddOrUpdatePictureImageData: length byteArrayTempImage: " + byteArrayTempImage.length);
        try
        {
            // wenn es eien Picture ID gibt so wird eine Update (DELETE Flag setzten und anlegen) ausgeführt sonst ein INSERT!
            if (intPicId == null || intPicId < 1)
            {
                // Speichern des BIldes
                long newPicId = db.insert(ImageDBModel.TABLE_Pics, null, cv);
                // Es gibt keine INFO Data Einträge für Pictures nur für Tempaltes!
                //long newInfoId = this.AddOrUpdateInfoData((int)newTempId, strInfoTemp, intPicId);
                db.close();
                System.err.println("INFO: DataManager: AddOrUpdatePictureImageData: INSERT with image! newPicId: " + newPicId); // + " and Info Data: " + newInfoId);
                return (int) newPicId;
            }
            else
            {
                long retValueTemp = db.update(ImageDBModel.TABLE_Pics, cv, ImageDBModel.TABLE_Pics_COLUMN_id + "=?", new String[]{String.valueOf(intPicId)});
                //long retValueInfo = this.AddOrUpdateInfoData((int)intImageId, strInfoTemp, intInfoId);
                db.close();

                Integer retVal = 0;

                // Informationsausgabe!
                if (retValueTemp == 1) {
                    // Update erfolgreich!
                    System.err.println("DEBUG: DataManager: AddOrUpdatePictureImageData: UPDATE Pics with image! retValueTemp: " + retValueTemp);
                    retVal = (int) intPicId;
                }
                else {
                    // Update Fehlerhaft
                    System.err.println("DEBUG: DataManager: AddOrUpdatePictureImageData: UPDATE Pics mit Fehler! retValueTemp: " + retValueTemp);
                    retVal = -1;
                }
                /*
                if (retValueInfo == 1) {
                    // Update erfolgreich!
                    System.err.println("DEBUG: DataManager: AddOrUpdatePictureImageData: UPDATE Info OK! retValueInfo: " + retValueInfo);
                }
                else{
                    // Update Fehlerhaft
                    System.err.println("DEBUG: DataManager: AddOrUpdatePictureImageData: UPDATE Info mit Fehler! retValueInfo: " + retValueInfo);
                }
                */
                return retVal;
            }
        }
        catch(Exception strErr)
        {
            System.err.println("ERROR: DataManager: AddOrUpdatePictureImageData: " + strErr.toString());
            db.close();
            return -1;
        }
    }

    public int AddOrUpdateMatchingData(int newPicId, int strTempId, boolean boolMatched, int intMaReId){
        // Verknüpfe das Picture mit dem Template und hinterlegt die Matching Daten!
        long  newMaReId = 0;
        SQLiteDatabase db = imageDBAccess.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(ImageDBModel.TABLE_MaRe_COLUMN_p_id, newPicId);
        cv.put(ImageDBModel.TABLE_MaRe_COLUMN_t_id, strTempId);
        cv.put(ImageDBModel.TABLE_MaRe_COLUMN_matched, boolMatched);
        try
        {
            // Wenn es die ID zum Mapping Result nicht gibt wird dieses Eingefügt
            if (intMaReId < 1)
            {
                // Speichern des BIldes
                newMaReId = db.insert(ImageDBModel.TABLE_MaRe, null, cv);
                db.close();
                System.err.println("INFO: DataManager: AddOrUpdateMatchingData: INSERT newMaReId  " + newMaReId);
                return (int) newPicId;
            }
            else
            {
                long retValueTemp = db.update(ImageDBModel.TABLE_MaRe, cv, ImageDBModel.TABLE_MaRe_COLUMN_id + "=?", new String[]{String.valueOf(intMaReId)});
                db.close();
                Integer retVal = 0;

                // Informationsausgabe!
                if (retValueTemp == 1) {
                    // Update erfolgreich!
                    System.err.println("DEBUG: DataManager: AddOrUpdateMatchingData: UPDATE MaRe! retValueTemp: " + retValueTemp);
                    retVal = (int) intMaReId;
                }
                else {
                    // Update Fehlerhaft
                    System.err.println("DEBUG: DataManager: AddOrUpdateMatchingData: UPDATE MaRe! retValueTemp: " + retValueTemp);
                    retVal = -1;
                }
                return retVal;
            }
        }
        catch(Exception strErr)
        {
            System.err.println("ERROR: DataManager: AddOrUpdateMatchingData: " + strErr.toString());
            db.close();
            return -1;
        }
    }

    public ArrayList<String> deleteDataSet(int intDataSetID, String DataSetTABLENAME)
    {
        ArrayList<String> alRes = new ArrayList<String>();
        //löschen des Eintrages per Name der Tabelle und der ID des Datensatzes
        try{
            //löschen des Eintrages per Name der Tabelle und der ID des Datensatzes
            SQLiteDatabase db = imageDBAccess.getWritableDatabase();

            //String strDeleteQuery = "DELETE FROM " + ImageDBModel.TABLE_Temp + " WHERE " +
            //        ImageDBModel.TABLE_Temp_COLUMN_id + " = " + intDataSetID;
            String strDeleteQueryTemp = "UPDATE " + ImageDBModel.TABLE_Temp + " SET " +
                    ImageDBModel.TABLE_Temp_COLUMN_deleted + " = 1 WHERE " +
                    ImageDBModel.TABLE_Temp_COLUMN_id + " = " + intDataSetID;
            System.err.println("DEBUG: DataManager: deleteDataSet = " + strDeleteQueryTemp.toString());
            db.execSQL(strDeleteQueryTemp);

            String strDeleteQueryInfo = "UPDATE " + ImageDBModel.TABLE_Info + " SET " +
                    ImageDBModel.TABLE_Info_COLUMN_deleted + " = 1 WHERE " +
                    ImageDBModel.TABLE_Info_COLUMN_t_id + " = " + intDataSetID;
            System.err.println("DEBUG: DataManager: deleteDataSet = " + strDeleteQueryInfo.toString());
            db.execSQL(strDeleteQueryInfo);

            /* // Does not works!
            String strDeleteQuery = "DELETE FROM " + ImageDBModel.TABLE_Temp + " WHERE " + ImageDBModel.TABLE_Temp_COLUMN_id + "=?";
            System.err.println("ERROR: DataManager: doDeleteDataSet: strDeleteQuery = " + strDeleteQuery.toString());
            db.rawQuery(strDeleteQuery, new String[] {String.valueOf(intDataSetID) } );  // String representation of an interger!
            */

            db.close();
            alRes.add("0");
            alRes.add("Delete Item: " + intDataSetID + " from Table: "  + DataSetTABLENAME.toString() + "  OK");
            return alRes;
        }
        catch (Exception strErr)
        {
            System.err.println("ERROR: DataManager: deleteDataSet: " + strErr.toString());
            alRes.add("-2");
            alRes.add("Delete Item: " + intDataSetID + " from Table: "  + DataSetTABLENAME.toString() + "  Failed: " + strErr.toString());
            return alRes;
        }
    }

    public objMatchingResult getMatchingResult (int intMaReId)
    {
        // Laden der notwendigen Daten über die MaReId
        SQLiteDatabase db = imageDBAccess.getReadableDatabase();
        String strSelectQuery = "SELECT " +
            " Pics." + ImageDBModel.TABLE_Pics_COLUMN_path + ", " +
            " Pics." + ImageDBModel.TABLE_Pics_COLUMN_name + ", " +
            " Temp." + ImageDBModel.TABLE_Temp_COLUMN_name + ", " +
            " Temp." + ImageDBModel.TABLE_Temp_COLUMN_cont +
            " FROM " +
                ImageDBModel.TABLE_MaRe + " MaRe, " +
                ImageDBModel.TABLE_Pics + " Pics, " +
                ImageDBModel.TABLE_Temp + " Temp " +
            " WHERE MaRe." + ImageDBModel.TABLE_MaRe_COLUMN_id   + " =? " +
            "   AND MaRe." + ImageDBModel.TABLE_MaRe_COLUMN_p_id + " = Pics." + ImageDBModel.TABLE_Pics_COLUMN_id +
            "   AND MaRe." + ImageDBModel.TABLE_MaRe_COLUMN_t_id + " = Temp." + ImageDBModel.TABLE_Temp_COLUMN_id;

        System.err.println("DEBUG: DataManager: getMatchingResult: strSelectQuery = " + strSelectQuery.toString());
        Cursor curSelectQuery = db.rawQuery(strSelectQuery, new String[] {String.valueOf(intMaReId)} );

        // put data of first an only select result into the class
        objMatchingResult objMR = new objMatchingResult();
        if (curSelectQuery.moveToFirst()) {
            objMR.p_path = curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Pics_COLUMN_path));
            objMR.p_name = curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Pics_COLUMN_name));
            objMR.t_name = curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Temp_COLUMN_name));
            objMR.t_cont = curSelectQuery.getBlob(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Temp_COLUMN_cont));

            System.err.println("DEBUG: DataManager: getMatchingResult: objMR: " + objMR.p_path + "  " + objMR.p_name + "  " + objMR.t_name);
        }
        db.close();
        return objMR;
    }

    public objTEMPLATE getTemplateImageClassByIDFromDB(int intTemplateId, objTEMPLATE classObjTEMPLATE)
    {
        // Die Daten zu einem Template
        ArrayList<String> alPathName = new ArrayList<String>();
        //try {
        SQLiteDatabase db = imageDBAccess.getReadableDatabase();
        String strSelectQuery = "SELECT " +
                ImageDBModel.TABLE_Temp_COLUMN_name + ", " +
                ImageDBModel.TABLE_Temp_COLUMN_path + ", " +
                ImageDBModel.TABLE_Temp_COLUMN_cont +
                " FROM " + ImageDBModel.TABLE_Temp +
                " WHERE " + ImageDBModel.TABLE_Temp_COLUMN_id + "=?" +
                "   AND " + ImageDBModel.TABLE_Temp_COLUMN_deleted + " IS NOT ?";
        System.err.println("INFO: DataManager: getTemplateImageClassByIDFromDB: strSelectQueryt: " + strSelectQuery.toString() + " mit ID = " + intTemplateId);
        Cursor curSelectQuery = db.rawQuery(strSelectQuery, new String[] {String.valueOf(intTemplateId), "1".toString() } );  // String representation of an interger!
        //Cursor curSelectQuery = db.rawQuery(strSelectQuery, new String[] {templateId} );

        //        " WHERE " + ImageDBModel.TABLE_Temp_COLUMN_id + " = " + templateId;
        //Cursor curSelectQuery = db.rawQuery(strSelectQuery, null);

        if (curSelectQuery.moveToFirst()) {
            // wenn es einen erste Eintrag gibt so geht die Anwendung hier her!
            do {
                classObjTEMPLATE.t_name = curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Temp_COLUMN_name));
                classObjTEMPLATE.t_path = curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Temp_COLUMN_path));
                classObjTEMPLATE.t_cont = curSelectQuery.getBlob(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Temp_COLUMN_cont));

                System.err.println("INFO: DataManager: getTemplateImageClassByIDFromDB: length classObjTEMPLATE.t_cont: " + classObjTEMPLATE.t_cont.length);
            } while (curSelectQuery.moveToNext());
        }
        db.close();
        return classObjTEMPLATE;
        /*
        }
        catch (Exception strErr)
        {
            System.err.println("ERROR: DataManager: getTemplateImageDataByIDFromDB: " + strErr.toString());
             return classObjTEMPLATE;
        }
        return classObjTEMPLATE;
        */
    }

    public int AddOrUpdateInfoData(Integer intImageId, String strInfoData, Integer intInfoId)
    {
        // setzen eine valiten Integer Wert
        if (intInfoId == null) {intInfoId = 0;}

        SQLiteDatabase db = imageDBAccess.getWritableDatabase();
        //alle bisherigen auf DELETED = 1 setzen

        String strUpdateQuery = "UPDATE " + ImageDBModel.TABLE_Info +
                " SET " + ImageDBModel.TABLE_Info_COLUMN_deleted + " = 1 " +
                " WHERE " + ImageDBModel.TABLE_Info_COLUMN_t_id + " IS ? AND " + ImageDBModel.TABLE_Info_COLUMN_deleted + " IS NOT 1";
        System.err.println("INFO: DataManager: AddOrUpdateInfoData: strUpdateQuery: " + strUpdateQuery.toString() + " mit ID = " + intImageId);
        db.execSQL(strUpdateQuery, new String[] { String.valueOf(intImageId) } );  // String representation of an interger!
        db.close();
        // - anzeigen
        System.err.println("INFO: DataManager: AddOrUpdateInfoData => printInfoList....");
        this.printInfoList(intImageId);
        //System.err.println("INFO: DataManager: AddOrUpdateInfoData => printInfoList EVEN DELETE....");
        //this.printInfoListEvenDeleted(intImageId);

        // neuen Eintrag vornehmen!
        db = imageDBAccess.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(ImageDBModel.TABLE_Info_COLUMN_cont, strInfoData);
        cv.put(ImageDBModel.TABLE_Info_COLUMN_t_id, String.valueOf(intImageId));
        System.err.println("INFO: DataManager: AddOrUpdateInfoData: strInfoData=" + strInfoData.toString()  + " intImageId=" + intImageId + " intInfoId=" + intInfoId);
        try
        {
            if (intInfoId == 0)
            {
                long newInfoId = db.insert(ImageDBModel.TABLE_Info, null, cv);
                db.close();
                System.err.println("INFO: DataManager: AddOrUpdateInfoData: INSERT Info newInfoId: " + newInfoId);
                return (int) newInfoId;
            }
            else if (intInfoId > 0)
            {
                long retValue = db.update(ImageDBModel.TABLE_Info, cv, ImageDBModel.TABLE_Info_COLUMN_id + "=?", new String[]{String.valueOf(intInfoId)});
                db.close();
                System.err.println("INFO: DataManager: AddOrUpdateInfoData: UDPATE Info retValue: " + retValue);

                if (retValue == 1) {
                    // Update erfolgreich!
                    System.err.println("DEBUG: DataManager: AddOrUpdateInfoData: UPDATE Info mit Erfolg intInfoId: " + intInfoId);
                    return (int) intInfoId;
                }
                else
                {
                    // Update fehlerhaft
                    System.err.println("DEBUG: DataManager: AddOrUpdateInfoData: UPDATE Info mit Fehler! return mit -1");
                    return -1;
                }
            }
            else
            {
                System.err.println("DEBUG: DataManager: AddOrUpdateInfoData: UNBEKANNTER ZUSTAND");
                return -1;
            }
        }
        catch(Exception strErr)
        {
            System.err.println("ERROR: DataManager: addTemplateImageData: " + strErr.toString());
            db.close();
            return -1;
        }
    }

    public String printInfoList(int intTempId)
    {
        // Das Sammelobjekt fuer das Ergebnis
        String r_strTempInfo = "";
        System.err.println("DEBUG: DataManager: printInfoList: ... intTempId="  +intTempId);
        try {
            // Auslesen der ID und des Inhalt der Informations Images
            SQLiteDatabase db = imageDBAccess.getReadableDatabase();
            String strSelectQuery = "SELECT " +
                    ImageDBModel.TABLE_Info_COLUMN_id + ", " +
                    ImageDBModel.TABLE_Info_COLUMN_cont + ", " +
                    ImageDBModel.TABLE_Info_COLUMN_t_id + ", " +
                    ImageDBModel.TABLE_Info_COLUMN_deleted +
                    " FROM " + ImageDBModel.TABLE_Info +
                    " WHERE " + ImageDBModel.TABLE_Info_COLUMN_t_id  + " =? "  +
                    " AND " + ImageDBModel.TABLE_Info_COLUMN_deleted + " IS NOT ?"; // nur die nicht gelöschten Einträge zeigen!
            Cursor curSelectQuery = db.rawQuery(strSelectQuery, new String[] {String.valueOf(intTempId), "1"} );

            //Cursor curSelectQuery = db.rawQuery(strSelectQuery, null);

            if (curSelectQuery.moveToFirst()) {
                // wenn es einen erste Eintrag gibt so geht die Anwendung hier her!
                do {
                    HashMap<String, String> actTempImageDate = new HashMap<String, String>();
                    System.err.println("ERROR: " + ImageDBModel.TABLE_Info_COLUMN_id + " :" + curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Info_COLUMN_id)));
                    System.err.println("ERROR: -" + ImageDBModel.TABLE_Info_COLUMN_cont +" :" + curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Info_COLUMN_cont)));
                    System.err.println("ERROR: -" + ImageDBModel.TABLE_Info_COLUMN_t_id + " :" + curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Info_COLUMN_t_id)));
                    System.err.println("ERROR: -" + ImageDBModel.TABLE_Info_COLUMN_deleted + " :" + curSelectQuery.getInt(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Info_COLUMN_deleted)));
                    r_strTempInfo = curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Info_COLUMN_cont));

                } while (curSelectQuery.moveToNext());
            }
            db.close();
            return r_strTempInfo;
        }
        catch (Exception strErr)
        {
            System.err.println("ERROR: DataManager: printInfoList: "  + strErr.toString());
            return r_strTempInfo;
        }
    }

    public String printInfoListEvenDeleted(int intTempId)
    {
        // Das Sammelobjekt fuer das Ergebnis
        String r_strTempInfo = "";
        System.err.println("DEBUG: DataManager: printInfoList: ... intTempId="  +intTempId);
        try {
            // Auslesen der ID und des Inhalt der Informations Images
            SQLiteDatabase db = imageDBAccess.getReadableDatabase();
            String strSelectQuery = "SELECT " +
                    ImageDBModel.TABLE_Info_COLUMN_id + ", " +
                    ImageDBModel.TABLE_Info_COLUMN_cont + ", " +
                    ImageDBModel.TABLE_Info_COLUMN_t_id + ", " +
                    ImageDBModel.TABLE_Info_COLUMN_deleted +
                    " FROM " + ImageDBModel.TABLE_Info +
                    " WHERE " + ImageDBModel.TABLE_Info_COLUMN_t_id  + "=?";
            Cursor curSelectQuery = db.rawQuery(strSelectQuery, new String[] {String.valueOf(intTempId)} );

            //Cursor curSelectQuery = db.rawQuery(strSelectQuery, null);

            if (curSelectQuery.moveToFirst()) {
                // wenn es einen erste Eintrag gibt so geht die Anwendung hier her!
                do {
                    HashMap<String, String> actTempImageDate = new HashMap<String, String>();
                    System.err.println("ERROR: " + ImageDBModel.TABLE_Info_COLUMN_id + " :" + curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Info_COLUMN_id)));
                    System.err.println("ERROR: -" + ImageDBModel.TABLE_Info_COLUMN_cont +" :" + curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Info_COLUMN_cont)));
                    System.err.println("ERROR: -" + ImageDBModel.TABLE_Info_COLUMN_t_id + " :" + curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Info_COLUMN_t_id)));
                    System.err.println("ERROR: -" + ImageDBModel.TABLE_Info_COLUMN_deleted + " :" + curSelectQuery.getInt(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Info_COLUMN_deleted)));
                    r_strTempInfo = curSelectQuery.getString(curSelectQuery.getColumnIndex(ImageDBModel.TABLE_Info_COLUMN_cont));

                } while (curSelectQuery.moveToNext());
            }
            db.close();
            return r_strTempInfo;
        }
        catch (Exception strErr)
        {
            System.err.println("ERROR: DataManager: printInfoList: "  + strErr.toString());
            return r_strTempInfo;
        }
    }
}
