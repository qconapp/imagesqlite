package com.example.jlemmnitz.imagesqlite;

/**
 * Created by jlemmnitz on 30.03.2015.
 */

public class ImageDBModel {

    // Tablenames of DataModel
    public static final String TABLE_Info                       = "Information";
    public static final String TABLE_Temp                       = "Template";
    public static final String TABLE_Pics                       = "Picture";
    public static final String TABLE_MaRe                       = "Matching_Result";

    // Columns Header of Information Table
    public static final String TABLE_Info_COLUMN_id             = "i_id";
    public static final String TABLE_Info_COLUMN_url            = "i_url";
    public static final String TABLE_Info_COLUMN_desc           = "i_desc";
    public static final String TABLE_Info_COLUMN_cont           = "i_cont";
    public static final String TABLE_Info_COLUMN_t_id           = "t_id";
    public static final String TABLE_Info_COLUMN_deleted        = "i_deleted";
    // Column DataType of Information Table
    public static final String TABLE_Info_COLUMN_id_TYPE        = "INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final String TABLE_Info_COLUMN_url_TYPE       = "TEXT";
    public static final String TABLE_Info_COLUMN_desc_TYPE      = "TEXT";
    public static final String TABLE_Info_COLUMN_cont_TYPE      = "TEXT";  // Die Information..
    public static final String TABLE_Info_COLUMN_t_id_TYPE      = "INTEGER";
    public static final String TABLE_Info_COLUMN_deleted_TYPE   = "BOOLEAN DEFAULT 0";

    // Columns Header of Template Table
    public static final String TABLE_Temp_COLUMN_id             = "t_id";
    public static final String TABLE_Temp_COLUMN_path           = "t_path";
    public static final String TABLE_Temp_COLUMN_name           = "t_name";
    public static final String TABLE_Temp_COLUMN_desc           = "t_desc";
    public static final String TABLE_Temp_COLUMN_cont           = "t_cont";
    public static final String TABLE_Temp_COLUMN_type           = "t_type";
    public static final String TABLE_Temp_COLUMN_matinfo        = "t_matinfo";
    public static final String TABLE_Temp_COLUMN_deleted        = "t_deleted";

    // Column DataType of Template Table
    public static final String TABLE_Temp_COLUMN_id_TYPE        = "INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final String TABLE_Temp_COLUMN_path_TYPE      = "TEXT";
    public static final String TABLE_Temp_COLUMN_name_TYPE      = "TEXT";
    public static final String TABLE_Temp_COLUMN_desc_TYPE      = "TEXT";
    public static final String TABLE_Temp_COLUMN_cont_TYPE      = "BLOB";
    public static final String TABLE_Temp_COLUMN_type_TYPE      = "TEXT";
    public static final String TABLE_Temp_COLUMN_matinfo_TYPE   = "BLOB";
    public static final String TABLE_Temp_COLUMN_deleted_TYPE   = "BOOLEAN DEFAULT 0";

    // Columns Header of Pictures Table
    public static final String TABLE_Pics_COLUMN_id             = "p_id";
    public static final String TABLE_Pics_COLUMN_path           = "p_path";
    public static final String TABLE_Pics_COLUMN_name           = "p_name";
    public static final String TABLE_Pics_COLUMN_desc           = "p_desc";
    public static final String TABLE_Pics_COLUMN_type           = "p_type";
    public static final String TABLE_Pics_COLUMN_matinfo        = "p_matinfo";
    public static final String TABLE_Pics_COLUMN_deleted        = "p_deleted";

    // Column DataType of Pictures Table
    public static final String TABLE_Pics_COLUMN_id_TYPE        = "INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final String TABLE_Pics_COLUMN_path_TYPE      = "TEXT";
    public static final String TABLE_Pics_COLUMN_name_TYPE      = "TEXT";
    public static final String TABLE_Pics_COLUMN_desc_TYPE      = "TEXT";
    public static final String TABLE_Pics_COLUMN_type_TYPE      = "TEXT";
    public static final String TABLE_Pics_COLUMN_matinfo_TYPE   = "BLOB";
    public static final String TABLE_Pics_COLUMN_deleted_TYPE   = "BOOLEAN DEFAULT 0";

    // Columns Header of Matching_Result Table
    //  -> Das Pictues ist verknüpft mit dem Tenplate(s)
    public static final String TABLE_MaRe_COLUMN_id             = "mr_id";
    public static final String TABLE_MaRe_COLUMN_path           = "mr_path";
    public static final String TABLE_MaRe_COLUMN_name           = "mr_name";
    public static final String TABLE_MaRe_COLUMN_desc           = "mr_desc";
    public static final String TABLE_MaRe_COLUMN_type           = "mr_type";
    public static final String TABLE_MaRe_COLUMN_matched        = "mr_matched";
    public static final String TABLE_MaRe_COLUMN_matinfo        = "mr_matinfo";
    public static final String TABLE_MaRe_COLUMN_p_id           = "p_id";
    public static final String TABLE_MaRe_COLUMN_t_id           = "t_id";
    public static final String TABLE_MaRe_COLUMN_deleted        = "mr_deleted";


    // Column DataType of Matching_Result Table
    public static final String TABLE_MaRe_COLUMN_id_TYPE        = "INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final String TABLE_MaRe_COLUMN_path_TYPE      = "TEXT";
    public static final String TABLE_MaRe_COLUMN_name_TYPE      = "TEXT";
    public static final String TABLE_MaRe_COLUMN_desc_TYPE      = "TEXT";
    public static final String TABLE_MaRe_COLUMN_type_TYPE      = "TEXT";
    public static final String TABLE_MaRe_COLUMN_matched_TYPE   = "BOOLEAN DEFAULT 0";  // Matched false : keine Erfolgreiches Matching von Picture und Tempalte
    public static final String TABLE_MaRe_COLUMN_matinfo_TYPE   = "BLOB";
    public static final String TABLE_MaRe_COLUMN_p_id_TYPE      = "INTEGER";
    public static final String TABLE_MaRe_COLUMN_t_id_TYPE      = "INTEGER";
    public static final String TABLE_MaRe_COLUMN_deleted_TYPE   = "BOOLEAN DEFAULT 0";

    // weitere Variablen
    public byte[] byteArrayImage;
}
