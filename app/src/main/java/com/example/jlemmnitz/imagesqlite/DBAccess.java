package com.example.jlemmnitz.imagesqlite; /**
 * Created by jlemmnitz on 30.03.2015.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import com.example.jlemmnitz.imagesqlite.ImageDBModel;

public class DBAccess extends SQLiteOpenHelper
{
    // eigene Java Klassen importieren
    //ImageDBModel idbmInfo = new ImageDBModel();

    // ons created keep the database!
    private static final int DATABASE_VERSION = 1;
    // Name der Datenbank
    private static final String DATABASE_NAME = "imagesql.db";

    // MyDBHandler(Context context, String name,
    // (Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    public DBAccess(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // DB_VERSION is an int,update it every new build

        System.err.println("DBAccess: Cosntructuro: context: " + context.toString());
        System.err.println("DBAccess: Cosntructuro: DATABASE_NAME: " + DATABASE_NAME);
        System.err.println("DBAccess: Cosntructuro: DBPath : " + context.getDatabasePath(DATABASE_NAME).getAbsolutePath());

        // erzwinge das onCreate!
        //this.onCreate(this.getWritableDatabase());
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        // Anlegen der Datenbank Tabellen
        try
        {
            System.err.println("DEBUG: DBAccess: onCreate: Create CREATE_TABLE_Info");
            String CREATE_TABLE_Info = //"; " +
                "CREATE TABLE IF NOT EXISTS " + ImageDBModel.TABLE_Info + " (" +
                        ImageDBModel.TABLE_Info_COLUMN_id      + "  " + ImageDBModel.TABLE_Info_COLUMN_id_TYPE      + ", " +
                        ImageDBModel.TABLE_Info_COLUMN_url     + "  " + ImageDBModel.TABLE_Info_COLUMN_url_TYPE     + ", " +
                        ImageDBModel.TABLE_Info_COLUMN_desc    + "  " + ImageDBModel.TABLE_Info_COLUMN_desc_TYPE    + ", " +
                        ImageDBModel.TABLE_Info_COLUMN_cont    + "  " + ImageDBModel.TABLE_Info_COLUMN_cont_TYPE    + ", " +
                        ImageDBModel.TABLE_Info_COLUMN_t_id    + "  " + ImageDBModel.TABLE_Info_COLUMN_t_id_TYPE    + ", " +
                        ImageDBModel.TABLE_Info_COLUMN_deleted + "  " + ImageDBModel.TABLE_Info_COLUMN_deleted_TYPE + ")";
            db.execSQL(CREATE_TABLE_Info);

            System.err.println("DEBUG: DBAccess: onCreate: Create CREATE_TABLE_Temp");
            String CREATE_TABLE_Temp =
                    "CREATE TABLE IF NOT EXISTS " + ImageDBModel.TABLE_Temp + " (" +
                            ImageDBModel.TABLE_Temp_COLUMN_id      + "  " + ImageDBModel.TABLE_Temp_COLUMN_id_TYPE      + ", " +
                            ImageDBModel.TABLE_Temp_COLUMN_path    + "  " + ImageDBModel.TABLE_Temp_COLUMN_path_TYPE    + ", " +
                            ImageDBModel.TABLE_Temp_COLUMN_name    + "  " + ImageDBModel.TABLE_Temp_COLUMN_name_TYPE    + ", " +
                            ImageDBModel.TABLE_Temp_COLUMN_desc    + "  " + ImageDBModel.TABLE_Temp_COLUMN_desc_TYPE    + ", " +
                            ImageDBModel.TABLE_Temp_COLUMN_cont    + "  " + ImageDBModel.TABLE_Temp_COLUMN_cont_TYPE    + ", " +
                            ImageDBModel.TABLE_Temp_COLUMN_type    + "  " + ImageDBModel.TABLE_Temp_COLUMN_type_TYPE    + ", " +
                            ImageDBModel.TABLE_Temp_COLUMN_matinfo + "  " + ImageDBModel.TABLE_Temp_COLUMN_matinfo_TYPE + ", " +
                            ImageDBModel.TABLE_Temp_COLUMN_deleted + "  " + ImageDBModel.TABLE_Temp_COLUMN_deleted_TYPE + ")";
            db.execSQL(CREATE_TABLE_Temp);

            System.err.println("DEBUG: DBAccess: onCreate: Create CREATE_TABLE_Pics");
            String CREATE_TABLE_Pics =
                    "CREATE TABLE IF NOT EXISTS " + ImageDBModel.TABLE_Pics + " (" +
                            ImageDBModel.TABLE_Pics_COLUMN_id      + "  " + ImageDBModel.TABLE_Pics_COLUMN_id_TYPE      + ", " +
                            ImageDBModel.TABLE_Pics_COLUMN_path    + "  " + ImageDBModel.TABLE_Pics_COLUMN_path_TYPE    + ", " +
                            ImageDBModel.TABLE_Pics_COLUMN_name    + "  " + ImageDBModel.TABLE_Pics_COLUMN_name_TYPE    + ", " +
                            ImageDBModel.TABLE_Pics_COLUMN_desc    + "  " + ImageDBModel.TABLE_Pics_COLUMN_desc_TYPE    + ", " +
                            ImageDBModel.TABLE_Pics_COLUMN_type    + "  " + ImageDBModel.TABLE_Pics_COLUMN_type_TYPE    + ", " +
                            ImageDBModel.TABLE_Pics_COLUMN_matinfo + "  " + ImageDBModel.TABLE_Pics_COLUMN_matinfo_TYPE + ", " +

                            ImageDBModel.TABLE_Pics_COLUMN_deleted + "  " + ImageDBModel.TABLE_Pics_COLUMN_deleted_TYPE + ")";
            db.execSQL(CREATE_TABLE_Pics);

            System.err.println("DEBUG: DBAccess: onCreate: Create CREATE_TABLE_MaRe");
            String CREATE_TABLE_MaRe =
                    "CREATE TABLE IF NOT EXISTS " + ImageDBModel.TABLE_MaRe + " (" +
                            ImageDBModel.TABLE_MaRe_COLUMN_id      + "  " + ImageDBModel.TABLE_MaRe_COLUMN_id_TYPE      + ", " +
                            ImageDBModel.TABLE_MaRe_COLUMN_path    + "  " + ImageDBModel.TABLE_MaRe_COLUMN_path_TYPE    + ", " +
                            ImageDBModel.TABLE_MaRe_COLUMN_name    + "  " + ImageDBModel.TABLE_MaRe_COLUMN_name_TYPE    + ", " +
                            ImageDBModel.TABLE_MaRe_COLUMN_desc    + "  " + ImageDBModel.TABLE_MaRe_COLUMN_desc_TYPE    + ", " +
                            ImageDBModel.TABLE_MaRe_COLUMN_type    + "  " + ImageDBModel.TABLE_MaRe_COLUMN_type_TYPE    + ", " +
                            ImageDBModel.TABLE_MaRe_COLUMN_matched + "  " + ImageDBModel.TABLE_MaRe_COLUMN_matched_TYPE  + ", " +
                            ImageDBModel.TABLE_MaRe_COLUMN_matinfo + "  " + ImageDBModel.TABLE_MaRe_COLUMN_matinfo_TYPE + ", " +
                            ImageDBModel.TABLE_MaRe_COLUMN_p_id    + "  " + ImageDBModel.TABLE_MaRe_COLUMN_p_id_TYPE    + ", " +
                            ImageDBModel.TABLE_MaRe_COLUMN_t_id    + "  " + ImageDBModel.TABLE_MaRe_COLUMN_t_id_TYPE    + ", " +
                            ImageDBModel.TABLE_MaRe_COLUMN_deleted + "  " + ImageDBModel.TABLE_MaRe_COLUMN_deleted_TYPE + ")";
            db.execSQL(CREATE_TABLE_MaRe);

            // Indexe anlegen!
            String str_CREATE_INDEX = "";
            str_CREATE_INDEX = "CREATE INDEX IF NOT EXISTS idx" + ImageDBModel.TABLE_Temp_COLUMN_name   + " ON "  + ImageDBModel.TABLE_Temp + " (" + ImageDBModel.TABLE_Temp_COLUMN_name + ")";
            db.execSQL(str_CREATE_INDEX);
            str_CREATE_INDEX = "CREATE INDEX IF NOT EXISTS idx" + ImageDBModel.TABLE_Pics_COLUMN_name   + " ON "  + ImageDBModel.TABLE_Pics + " (" + ImageDBModel.TABLE_Pics_COLUMN_name + ")";
            db.execSQL(str_CREATE_INDEX);
            str_CREATE_INDEX = "CREATE INDEX IF NOT EXISTS idx" + ImageDBModel.TABLE_MaRe_COLUMN_p_id   + " ON "  + ImageDBModel.TABLE_MaRe + " (" + ImageDBModel.TABLE_MaRe_COLUMN_p_id + ")";
            db.execSQL(str_CREATE_INDEX);
            str_CREATE_INDEX = "CREATE INDEX IF NOT EXISTS idx" + ImageDBModel.TABLE_MaRe_COLUMN_t_id   + " ON "  + ImageDBModel.TABLE_MaRe + " (" + ImageDBModel.TABLE_MaRe_COLUMN_t_id + ")";
            db.execSQL(str_CREATE_INDEX);
            str_CREATE_INDEX = "CREATE INDEX IF NOT EXISTS idx" + ImageDBModel.TABLE_MaRe_COLUMN_matched + " ON "  + ImageDBModel.TABLE_MaRe + " (" + ImageDBModel.TABLE_MaRe_COLUMN_matched + ")";
            db.execSQL(str_CREATE_INDEX);

        }
        catch (Exception strErr)
        {
            System.err.println("ERROR: DBAccess: onCreate: " + strErr.toString());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        // rebuild the Database
        // not realy needed
        /*
        db.execSQL("DROP INDEX IF EXISTS  idx" + ImageDBModel.TABLE_Temp_COLUMN_name);
        this.onCreate(db);
        db.execSQL("DROP INDEX IF EXISTS  idx" + ImageDBModel.TABLE_Pics_COLUMN_name);
        this.onCreate(db);
        db.execSQL("DROP INDEX IF EXISTS  idx" + ImageDBModel.TABLE_MaRe_COLUMN_p_id);
        this.onCreate(db);
        db.execSQL("DROP INDEX IF EXISTS  idx" + ImageDBModel.TABLE_MaRe_COLUMN_t_id);
        this.onCreate(db);
        db.execSQL("DROP INDEX IF EXISTS  idx" + ImageDBModel.TABLE_MaRe_COLUMN_matched);
        this.onCreate(db);
        */
        db.execSQL("DROP TABLE IF EXISTS  " + ImageDBModel.TABLE_Info);
        this.onCreate(db);
        db.execSQL("DROP TABLE IF EXISTS  " + ImageDBModel.TABLE_Temp);
        this.onCreate(db);
        db.execSQL("DROP TABLE IF EXISTS  " + ImageDBModel.TABLE_Pics);
        this.onCreate(db);
        db.execSQL("DROP TABLE IF EXISTS  " + ImageDBModel.TABLE_MaRe);
        this.onCreate(db);
    }

    // eigene Funktionen
    /*
    public int toAddUpdateInfo(Integer intTempId, String strURL, String strDesc, String strCont)
    {
        // prüfen ob ein Eintrag vorliegt: wenn ja dann Update sonst hinzufügen
        //db.execSQL("SELECT t_id FROM Information WHERE t_id = " + intTempId);
        try {

            // Erweiterung der Informationenstabelle
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            if (strURL != null) {
                cv.put(ImageDBModel.TABLE_Info_COLUMN_id, strURL);
            }
            if (strDesc != null) {
                cv.put(ImageDBModel.TABLE_Info_COLUMN_desc, strDesc);
            }
            if (strCont != null) {
                cv.put(ImageDBModel.TABLE_Info_COLUMN_cont, strCont);
            }
            if (intTempId != null) {
                cv.put(ImageDBModel.TABLE_Info_COLUMN_t_id, intTempId);
            }
            long retVal = db.insert(ImageDBModel.TABLE_Info, null, cv);
            System.err.println("INFO: DBAccess: toAddUpdateInfo: retVal = " + retVal);
            db.close();
            return (int)retVal;
        }
        catch (Exception strErr)
        {
            System.err.println("ERROR: DBAccess: toAddUpdateInfo: " + strErr.toString());
            return -1;
        }

    }
    */

}
