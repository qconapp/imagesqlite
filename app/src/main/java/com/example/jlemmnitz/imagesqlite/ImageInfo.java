package com.example.jlemmnitz.imagesqlite;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;


public class ImageInfo extends ActionBarActivity implements android.view.View.OnClickListener
{
    Button btnSave, btnDelete, btnCancel, changeImageForDatabase;
    EditText eTImagePath, eTImageName;
    ImageView iVdbImage;

    //Button btnSaveInfoData;
    EditText eTInfoData;

    // Variable fue die Datenbank
    public String strTableName = null;
    public String strImageId = null;
    public Integer intImageId = 0;
    public Integer intInfoId = 0;
    public String strImagePath = null;
    public String strImageName = null;
    public String strPicPathForImageView = null;
    public objTEMPLATE objTEMPLATEFromList = new objTEMPLATE();

    // true: anzeige der Ausgewähltn Bild daten!
    // false: Fenster ist leer!
    public boolean boolShowTempData = false;
    public String strShowTempData = "NULL";
    private static int RESULT_LOAD_IMAGE = 1;
    //private static int RESULT_IMAGE_INFO_Add = 900;
    //private static int RESULT_IMAGE_INFO_Update = 909;

    public ImageInfo()
    {
        super();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_info);
        // Objekt verknüpfen
        btnSave = (Button) findViewById(R.id.btnSave);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        changeImageForDatabase = (Button) findViewById(R.id.changeImageForDatabase);

        // OnClick ListenHandler anbinden
        btnSave.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        changeImageForDatabase.setOnClickListener(this);

        // Disable Save immer
        btnSave.setEnabled(false);
        btnDelete.setEnabled(false);

        iVdbImage = (ImageView) findViewById(R.id.iVdbImage);
        eTImageName = (EditText) findViewById(R.id.eTImageName);
        eTImagePath = (EditText) findViewById(R.id.eTImagePath);
        eTImageName.setEnabled(false);
        eTImagePath.setEnabled(false);

        // Save 2 Info Table
        //yId(R.id.btnSaveInfoData);
        //btnSaveInfoData.setOnClickListener(this);
        //btnSaveInfoData.setEnabled(false);
        //btnSaveInfoData.setCursorVisible(false);
        eTInfoData = (EditText) findViewById(R.id.eTInfoData);

        //try {
            // die Variable
            Bundle bdlVar = getIntent().getExtras();
            System.err.println("DEBUG:ImageInfo: onCreate: bdlVar: ... size: " + bdlVar.size());
            System.err.println("DEBUG:ImageInfo: onCreate: bdlVar: ... string: " + bdlVar.toString());

            if (bdlVar.size() > 0)
            {
                // diverse Parameter
                if (bdlVar.getString("boolShowTempData") != null) {
                    boolShowTempData = bdlVar.getBoolean("boolShowTempData");
                    System.err.println("DEBUG: ImageInfo: onCreate: -> boolShowTempData: " + bdlVar.getString("boolShowTempData").toString());
                }

                if (bdlVar.getString("strImageId") != null) {
                    strImageId = bdlVar.getString("strImageId");
                    System.err.println("DEBUG: ImageInfo: onCreate: -> strImageId: " + strImageId.toString());
                    // man geht davon aus das die ID auch mitgegebn wird1
                    objTEMPLATE objTEMPLATEFromDB = new objTEMPLATE();
                    intImageId = Integer.parseInt(strImageId);
                    System.err.println("DEBUG: ImageInfo: onCreate: -> intImageId: " + intImageId);

                    DataManager imageDBManager = new DataManager(this);
                    objTEMPLATEFromDB = imageDBManager.getTemplateImageClassByIDFromDB(intImageId, objTEMPLATEFromDB);
                    //if (objTEMPLATEFromDB != null) {
                    eTImageName.setText(objTEMPLATEFromDB.t_name);
                    eTImagePath.setText(objTEMPLATEFromDB.t_path);
                    // globale Variable füllen -> für das Update
                    strImageName = objTEMPLATEFromDB.t_name;
                    strImagePath = objTEMPLATEFromDB.t_path;
                    strPicPathForImageView = objTEMPLATEFromDB.t_path + objTEMPLATEFromDB.t_name;
                    System.err.println("DEBUG:ImageInfo: onCreate: -> objTEMPLATEFromDB strImageId : '" + strImageId.toString() + "' t_path: '" + objTEMPLATEFromDB.t_path.toString() + "' & t_name: '" + objTEMPLATEFromDB.t_name.toString() + "'");
                    System.err.println("DEBUG:ImageInfo: onCreate: -> objTEMPLATEFromDB t_cont length : " + objTEMPLATEFromDB.t_cont.length);

                    if (objTEMPLATEFromDB.t_cont.length > 0) {
                        ByteArrayInputStream imageStream = new ByteArrayInputStream(objTEMPLATEFromDB.t_cont);
                        iVdbImage.setImageBitmap(BitmapFactory.decodeStream(imageStream));
                    }
                    else
                    {
                        System.err.println("ERROR: ImageInfo: onCreate: -> objTEMPLATEFromDB is null");
                    }
                    eTInfoData.setText(imageDBManager.printInfoList(intImageId));
                }

                if (bdlVar.getString("strTableName") != null) {
                    strTableName = bdlVar.getString("strTableName");
                    System.err.println("DEBUG:ImageInfo: onCreate: -> strTableName: " + strTableName.toString());
                }
                if (bdlVar.getString("strImageId") != null) {
                    strImageId = bdlVar.getString("strImageId");
                    System.err.println("DEBUG:ImageInfo: onCreate: -> strImageId: " + strImageId.toString());
                    btnDelete.setEnabled(true);
                }
                if (bdlVar.getString("strImageName") != null) {
                    strImageName = bdlVar.getString("strImageName");
                    System.err.println("DEBUG:ImageInfo: onCreate: -> strImageName: " + strImageName.toString());
                }
                if (bdlVar.getString("btnAddUpdateImageTitle") != null) {
                    changeImageForDatabase.setText(bdlVar.getString("btnAddUpdateImageTitle"));
                    System.err.println("DEBUG:ImageInfo: onCreate: -> btnAddUpdateImageTitle: " + bdlVar.getString("btnAddUpdateImageTitle"));
                }

            }
        /*
        } catch (Exception strErr) {
            System.err.println( "ERROR ImageInfo OnCreate 1: putExtra Var:" + strErr.toString());
            //Toast.makeText(this, "ERROR ImageInfo OnCreate 1: putExtra Var:" + strErr.toString(), Toast.LENGTH_LONG).show();
        }*/

        // Start diverse Prozess wie laden von Objekten!
        //if (boolShowTempData == true)
        /*
        System.err.println("DEBUG:ImageInfo: onCreate: -> TEST strShowTempData: '" + strShowTempData.toString() + "'");
        if (strShowTempData == "EINS")
        {
            //objTEMPLATE objTEMPLATEFromList = imageDBAccess.getTemplateImageClassByIDFromDB(strImageId);
            objTEMPLATE objTEMPLATEFromList = imageDBAccess.getTemplateImageClassByIDFromDB("5");
            if (objTEMPLATEFromList != null) {
                eTImagePath.setText(objTEMPLATEFromList.t_path);
                eTImageName.setText(objTEMPLATEFromList.t_name);
                ByteArrayInputStream imageStream = new ByteArrayInputStream(objTEMPLATEFromList.t_cont);
                System.err.println("DEBUG:ImageInfo: onCreate: ->objTEMPLATEFromList strImageId : '"+ strImageId.toString() +"' t_path: '" + objTEMPLATEFromList.t_path.toString() + "' & t_name: '" + objTEMPLATEFromList.t_name.toString() + "'");
                iVdbImage.setImageBitmap(BitmapFactory.decodeStream(imageStream));
            }
            else
            {
                System.err.println("ERROR: ImageInfo: onCreate: -> objTEMPLATEFromList is null");
            }

        }
        else
        {
            System.err.println("DEBUG: ***** -> objTEMPLATEFromList ....strShowTempData '" + strShowTempData.toString() + "'");
        }
        */
        // jetzt nach dem Füllen des EditText Feldes den Listener einfügen
        eTInfoData.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                btnSave.setEnabled(true);
                //btnSaveInfoData.setEnabled(true);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_menue_main_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view)
    {
        // die Clicks...
        if (view == findViewById(R.id.changeImageForDatabase))
        {
            Toast.makeText(this, "DEBUG: ImageInfo: onClick: Add / Update Image", Toast.LENGTH_LONG).show();
            // Wenn der Parameter "" auf 1 gesetzt ist so kann die Gallery sofort gestartet werden
            // Ein neues Bild aus der Gallery laden und in die DB einfügen
            try {
                Intent itGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(itGallery, RESULT_LOAD_IMAGE);
                // Details der startActivityForResult() wird in der Funktion onActivityResult() vorgenommen
            } catch (Exception strErr) {
                Toast.makeText(this, "ERROR ImageInfo: onClick: " + strErr.toString(), Toast.LENGTH_LONG).show();
                System.err.println("ERROR ImageInfo: onClick: " + strErr.toString());
            }
        }
        else if (view == findViewById(R.id.btnSave))
        {
            //int RESULT_IMAGE_INFO_AddOrUpdate = 0;
            //try {
                // Die Info-Angaben zum Template Image
                String strInfoData = eTInfoData.getText().toString();
                // Die Angabe von Pfad und Name


                if (strImagePath == null || strImageName == null || strPicPathForImageView == null) {
                    if (strInfoData != null)
                    {
                        Toast.makeText(this, "WARNING: Can not save Information Data without Template Data.", Toast.LENGTH_LONG).show();
                        System.err.println("WARNING: ImageInfo: btnSave: Can not save Information Data without Template Data");
                    }
                    else {
                        Toast.makeText(this, "INFO: Please add Template Image and Information first.", Toast.LENGTH_LONG).show();
                        System.err.println("INFO: ImageInfo: btnSave: Please add Template Image and Information first.");
                    }
                } else {

                    Toast.makeText(this, "DEBUG: ImageInfo: Save", Toast.LENGTH_LONG).show();
                    System.err.println("DEBUG: ImageInfo: btnSave");
                    // Speichern der Informationen in die Datenbank

                    // der Bild Inhalt
                    if (iVdbImage.getDrawable() == null) {
                        System.err.println("DEBUG: ImageInfo: btnSave: getDrawable() is NONE: strPicPathForImageView=" + strPicPathForImageView.toString());
                        strPicPathForImageView = null;
                    }

                    DataManager imageDBManager = new DataManager(this);
                    ArrayList<String> resAL = imageDBManager.AddOrUpdateTemplateImageData(strImagePath, strImageName, null, null, strPicPathForImageView, intImageId, strInfoData, intInfoId);
                    int resVal = Integer.parseInt(resAL.get(0));
                    String resMsg = resAL.get(1);

                    Toast.makeText(this, "INFO: ImageInfo: btnSave: " + resMsg.toString(), Toast.LENGTH_LONG).show();

                    if (resVal > 0) {
                        Intent returnFromSave = new Intent();
                        returnFromSave.putExtra("ReturnFrom", "SAVE");
                        returnFromSave.putExtra("DoNext", "UPDATELISTE");
                        setResult(RESULT_OK, returnFromSave); // resultCode!
                        System.err.println("DEBUG: ImageInfo: btnSave: returnFromSave=" + returnFromSave.getExtras().toString());
                        btnSave.setEnabled(false);
                        finish();
                    }
                }
            /*}
            catch(Exception strErr)
            {
                System.err.println("ERROR: ImageInfo: btnSave: " + strErr.toString());
            }*/
        }
        else  if (view == findViewById(R.id.btnDelete))
        {
            if (intImageId > 0 && strTableName != null) {
                Toast.makeText(this, "DEBUG: ImageInfo: Delete", Toast.LENGTH_LONG).show();
                System.err.println("DEBUG: ImageInfo: Delete");

                // Löschen des offnen Eintrages: dazu braucht man den Tabellennanem und die ID!
                DataManager imageDBManager = new DataManager(this);
                System.err.println("DEBUG: ImageInfo: btnDelete: intImageId=" + intImageId + " strTableName=" + strTableName.toString());
                ArrayList<String> alDelRes = new ArrayList<String>();
                alDelRes = imageDBManager.deleteDataSet(intImageId, strTableName);
                if (Integer.parseInt(alDelRes.get(0)) < 0){
                    // Fehler beim Löschen
                    Toast.makeText(this, "ERROR: ImageInfo: Delete: " + alDelRes.get(0).toString(), Toast.LENGTH_LONG).show();
                }else {

                    Toast.makeText(this, "INFO: ImageInfo: Delete id=" + intImageId + " OK.", Toast.LENGTH_LONG).show();
                    // Rueckagabe werden an die main activity
                    Intent returnFromDelete = new Intent();
                    returnFromDelete.putExtra("ReturnFrom", "DELETE");
                    returnFromDelete.putExtra("DoNext", "UPDATELISTE");
                    setResult(RESULT_OK, returnFromDelete); // resultCode!
                    System.err.println("DEBUG: ImageInfo: btnDelete: returnFromDelete=" + returnFromDelete.getExtras().toString());
                    finish();
                }
            }
            else
            {
                Toast.makeText(this, "DEBUG: ImageInfo: Delete: Nothing to delete", Toast.LENGTH_LONG).show();
                System.err.println("DEBUG: ImageInfo: Delete: Nothing to delete");
            }
        }
        else  if (view == findViewById(R.id.btnCancel))
        {
            System.err.println("DEBUG: ImageInfo: Cancel!");
            Toast.makeText(this, "DEBUG: ImageInfo: Cancel", Toast.LENGTH_LONG).show();
            // Anpassung abbrechen

            Intent returnFromCancel = new Intent();
            returnFromCancel.putExtra("ReturnFrom", "CANCEL");
            returnFromCancel.putExtra("DoNext", "UPDATELISTE");
            setResult(RESULT_OK, returnFromCancel); // resultCode!
            System.err.println("DEBUG: ImageInfo: btnCancel: returnFromDelete=" + returnFromCancel.getExtras().toString());
            finish();
        }
        /*
        else  if (view == findViewById(R.id.btnSaveInfoData)) {
            // Speichern von Extra Infos, Test nur an existieren TempDaten
            String strInfoData = eTInfoData.getText().toString();
            if (intImageId == 0 && strInfoData != null)
            {
                Toast.makeText(this, "WARNING: Can not save Information Data without given Template Data.", Toast.LENGTH_LONG).show();
                System.err.println("WARNING: ImageInfo: btnSaveInfoData: Can not save Information Data without given Template Data");
            }
            else  if (intImageId > 0 && strInfoData != null)
            {
                DataManager imageDBManager = new DataManager(this);
                int resVal = imageDBManager.AddOrUpdateInfoData(intImageId, strInfoData, intInfoId);
            }
            else
            {
                Toast.makeText(this, "WARNING:Unknown Status.", Toast.LENGTH_LONG).show();
                System.err.println("WARNING: ImageInfo: btnSaveInfoData: Unknown Status");
            }
        }*/
        else
        {
            Toast.makeText(this, "INFO: ImageInfo: no Action", Toast.LENGTH_LONG);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent itData) {
        super.onActivityResult(requestCode, resultCode, itData);

        // Die Ergebnisse der Aktion der Gallery werden hier verarbeitet
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && itData != null) {
            try {
                // Objekt auswerten
                Uri selectedImage = itData.getData();
                String[] strArrFilePathColumn = {MediaStore.Images.Media.DATA};
                Cursor curGal = getContentResolver().query(selectedImage, strArrFilePathColumn, null, null, null);
                curGal.moveToFirst();
                int intColumnIndex = curGal.getColumnIndex(strArrFilePathColumn[0]);
                strPicPathForImageView = curGal.getString(intColumnIndex);
                curGal.close();

                // Anzeigen des Bildes
                iVdbImage.setImageBitmap(BitmapFactory.decodeFile(strPicPathForImageView));

                // Bildauflösung?
                //Toast.makeText(this, "WH:"+ iVdbImage.getMaxWidth() + " : " + iVdbImage.getMaxHeight(), Toast.LENGTH_LONG).show();

                // Angaben Pfad und Name
                String strPathFileName = strPicPathForImageView.toString();
                // aufteilen des Strings und ablegen auf globalend Variablen
                String[] strSplit = strPathFileName.split("/");
                strImageName = strSplit[strSplit.length - 1];
                strImagePath = strPathFileName.substring(0, strPathFileName.length() - strImageName.length());

                // Anzeigen von Pfad und Name
                eTImageName.setText(strImageName);
                eTImagePath.setText(strImagePath);

                Toast.makeText(this, "INFO ImageInfo: Gallery: strPicPath: '" + strPicPathForImageView.toString() + "'", Toast.LENGTH_LONG).show();
                // Wieder freischalten!
                btnSave.setEnabled(true);

            } catch (Exception strErr) {
                Toast.makeText(this,"ERROR MainSQLite: Fehler beim Laden aus der Gallery:" + strErr.toString() , Toast.LENGTH_LONG).show();
                //System.err.println("ERROR MainSQLite: Fehler beim Laden aus der Gallery:" + strErr.toString());
            }
        }
    }
}
