package com.example.jlemmnitz.imagesqlite;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.ArrayList;


public class ListViewFrame extends ActionBarActivity implements android.view.View.OnClickListener {

    ListView lVTempNames;
    ArrayList<String> alSQLLiteCont = new ArrayList<String>();
    ArrayList<String> alTempPathName = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_frame);

        Bundle getBdEV = getIntent().getExtras();
        System.err.println("DEBUG: ListViewFrame: oneCreate: Bundle Extras: " + getBdEV.toString());


        // Auslesen der Template Einträge aus der Datenbank
        DataManager imageDBManager = new DataManager(this);
        alSQLLiteCont = imageDBManager.getTemplateImageList();
        // wenn nicht leer!
        if (alSQLLiteCont.isEmpty()) {
            Toast.makeText(this, "WARNING: No Template Images found!", Toast.LENGTH_SHORT);
            System.err.println("DEBUG: ListViewFrame: oneCreate: .... WARNING: No Template Images found!");
        }

        // Ausgabe dieser in der Liste
        //alSQLLiteCont.add("1:qwertzuio");
        //alSQLLiteCont.add("2:342rtzuio");

        lVTempNames = (ListView) findViewById(R.id.lVTempNames);
        lVTempNames.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, alSQLLiteCont));
        lVTempNames.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            // der Listener bekommt nu einen onItemClick Handler!
            public void onItemClick(AdapterView<?> parent, View v, int position, long longObjectID) {
                onItemClickAction(v, position, longObjectID);
            }
        });
    }

    public void onClick(View view) {
        // die Clicks...
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_view_frame, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    public void onItemClickAction(View listeObject, int itemPosition, long longObjectID)
    {
        // Verarbeitung eines CLicks auf dem Eintrag einer Liste
        Toast.makeText(this, "DEBUG: ListViewFrame: onItemCllick", Toast.LENGTH_LONG).show();
        // weder ie ID der View nch die ObjectID aus dem onItemClick Listene reichen zum Abgleich

        // Lesen den Eintrag und Splitte ImageIdName in ID und NAME
        String strTempIdName = lVTempNames.getAdapter().getItem(itemPosition).toString();
        Toast.makeText(this, "LVF: INFO1: " + strTempIdName, Toast.LENGTH_LONG).show();
        String[] strArr_Id_Name = strTempIdName.split(":");

        // aufrufen der anderen Activity
        if (strArr_Id_Name.length == 2) {
            //DataManager imageDBManager = new DataManager(this);
            //alTempPathName = imageDBManager.getTemplateImagePathNameByID(strArr_Id_Name[0]);

            // Rückgabeparameter - Listeneintrag!
            Intent returnFromList = new Intent();
            System.err.println("DEBUG: ListViewFrame: strArr_Id_Name: " + strArr_Id_Name.toString());
            returnFromList.putExtra("strTempId", strArr_Id_Name[0].toString());
            returnFromList.putExtra("strTempName", strArr_Id_Name[1].toString());
            //returnFromList.putExtra("strTempPath", alTempPathName.get(0));
            //returnFromList.putExtra("strTempName", alTempPathName.get(1));
            setResult(RESULT_OK, returnFromList); // resultCode!
            System.err.println("DEBUG: ListViewFrame: return List Data...");
        }
        else
        {
            // alles 0!
            Intent returnFromList = new Intent();
            returnFromList.putExtra("strTempId", "");
            returnFromList.putExtra("strTempName", "");
            // füllen den Pfad aus mit Informationen aus der  Datenbank die anfangs gelesen wurden!
            //returnFromList.putExtra("strTempPath", "");
            setResult(RESULT_CANCELED, returnFromList);
            System.err.println("DEBUG: ListViewFrame: ListItem has no Data...");
            Toast.makeText(this, "ListItem has no Data':'", Toast.LENGTH_LONG).show();
        }
        finish();
    }
}
