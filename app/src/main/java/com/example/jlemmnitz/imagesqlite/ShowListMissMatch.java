package com.example.jlemmnitz.imagesqlite;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;


public class ShowListMissMatch extends ActionBarActivity implements android.view.View.OnClickListener{

    Button btnReturnToMain;
    EditText eTTitle;
    ListView lVMissMatch;
    ArrayList<String> alMatchingResults;
    Boolean boolMatched = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_list_miss_match);

        btnReturnToMain = (Button) findViewById(R.id.btnReturnToMain);
        btnReturnToMain.setOnClickListener(this);
        eTTitle = (EditText) findViewById(R.id.eTTitle);
        lVMissMatch = (ListView) findViewById(R.id.lVMissMatch);

        Bundle bdExtra = getIntent().getExtras();
        if (bdExtra.getString("Title") != null) {
            eTTitle.setText(bdExtra.getString("Title").toString());
        }
        boolMatched = bdExtra.getBoolean("Matched");

        // Auslesen der Template Einträge aus der Datenbank
        DataManager imageDBManager = new DataManager(this);
        alMatchingResults = imageDBManager.getMatchingResultsList(boolMatched);
        // wenn nicht leer!
        if (alMatchingResults.isEmpty()) {
            Toast.makeText(this, "WARNING: No Matching Results found!", Toast.LENGTH_SHORT);
            System.err.println("DEBUG: ShowListMissingMatch: oneCreate: .... WARNING: No Matching Results found!!");
        }
        else
        {
           // 1:Pic:Temp
            lVMissMatch = (ListView) findViewById(R.id.lVMissMatch);
            lVMissMatch.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, alMatchingResults));
            lVMissMatch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                // der Listener bekommt nu einen onItemClick Handler!
                public void onItemClick(AdapterView<?> parent, View v, int position, long longObjectID) {
                    onItemClickAction(v, position, longObjectID);
                }
            });
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_list_miss_match, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view){
        if (view == findViewById(R.id.btnReturnToMain))
        {
            finish();
        }
    }

    public void onItemClickAction(View listeObject, int itemPosition, long longObjectID)
    {
        // Verarbeitung eines CLicks auf dem Eintrag einer Liste
        Toast.makeText(this, "DEBUG: ShowListMissMatch: onItemClickAction", Toast.LENGTH_LONG).show();
        // weder ie ID der View nch die ObjectID aus dem onItemClick Listene reichen zum Abgleich

        // Lesen den Eintrag und Splitte ImageIdName in ID und NAME
        String strTempIdName = lVMissMatch.getAdapter().getItem(itemPosition).toString();
        String[] strArr_Id_Name = strTempIdName.split(":");

        // aufrufen der anderen Activity
        if (strArr_Id_Name.length == 3) {
            // Anzeige von Pic und Temp!
            Intent itSPOMM = new Intent (this, ShowPicOfMissMatch.class);
            itSPOMM.putExtra("intMaReId"   , Integer.parseInt(strArr_Id_Name[0].toString()));
            System.err.println("DEBUG: ShowListMissMatch: Item: " + strArr_Id_Name[0].toString() );
            startActivity(itSPOMM);
        }
        else
        {
            // alles 0!
            Toast.makeText(this, "WARNING: ShowListMissMatch: ListItem has no Data...", Toast.LENGTH_LONG).show();
            System.err.println("WARNING: ShowListMissMatch: ListItem has no Data...");
        }
    }
}
