package com.example.jlemmnitz.imagesqlite;

/**
 * Created by jlemmnitz on 01.04.2015.
 */
public class objTEMPLATE {

    // Alle Attribute der Tabelle Template sind hier gegeben.
    // Die Daten eines Datensatzes wede angefügt!

    // Name der Tabelle
    public static String TABLE_NAME = "Template";

    // Daten zum Bild
    public static String t_id = null;
    public static String t_path = null;
    public static String t_name = null;
    public static String t_desc = null;
    public static String t_type = null;

    // Das Bild selber
    public static byte[] t_cont = null;
    public static byte[] t_matinfo = null;
}