package com.example.jlemmnitz.imagesqlite;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayInputStream;


public class MatchingActivity extends ActionBarActivity  implements android.view.View.OnClickListener {

    Button btnGetPicFromGallery, btnGetTempFromDB, btnTrueMatching, btnDoesNotMatch, btnReturn;
    ImageView iVShowPic, iVShowTemp;
    TextView tVPicName, tVTempName;

    private static int RESULT_LOAD_IMAGE = 1;
    private static int RESULT_TEMPLATE_INFO = 888;
    public String strPicPathForImageView = null;
    public String strPicPath = null;
    public String strPicName = null;
    public String strTempPath = null;
    public String strTempName = null;
    public String strTempId = null;
    public int newPicId = 0;
    public int newMaReId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matching);

        btnGetPicFromGallery = (Button) findViewById(R.id.btnGetPicFromGallery);
        btnGetPicFromGallery.setOnClickListener(this);
        btnGetTempFromDB = (Button) findViewById(R.id.btnGetTempFromDB);
        btnGetTempFromDB.setOnClickListener(this);
        btnTrueMatching = (Button) findViewById(R.id.btnTrueMatching);
        btnTrueMatching.setOnClickListener(this);
        btnDoesNotMatch = (Button) findViewById(R.id.btnDoesNotMatch);
        btnDoesNotMatch.setOnClickListener(this);
        iVShowPic = (ImageView) findViewById(R.id.iVShowPic);
        iVShowTemp = (ImageView) findViewById(R.id.iVShowTemp);
        tVPicName = (TextView) findViewById(R.id.tVPicName);
        tVTempName = (TextView) findViewById(R.id.tVTempName);
        btnReturn = (Button) findViewById(R.id.btnReturn);
        btnReturn.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_matching, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {
        // die Clicks...
        if (view == findViewById(R.id.btnGetPicFromGallery)) {
            // Picture aus Gallery laden und anzeigen

            try {
                Intent itGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(itGallery, RESULT_LOAD_IMAGE);
            } catch (Exception strErr) {
                Toast.makeText(this, "ERROR: MatchingActivity: Fehler beim Nutzen Gallery:" + strErr.toString(), Toast.LENGTH_LONG).show();
                System.err.println("ERROR: MatchingActivity: Fehler beim Nutzen Gallery:" + strErr.toString());
            }

        }
        else if (view == findViewById(R.id.btnGetTempFromDB))
        {
            // Start das Activity -> laden dei Template ID und Name aus Datenbank in der Activity
            Intent itListViewTempInfo = new Intent(this, ListViewFrame.class);
            itListViewTempInfo.putExtra("DEBUG", "TEMP");
            startActivityForResult(itListViewTempInfo, RESULT_TEMPLATE_INFO);
        }
        else if (view == findViewById(R.id.btnTrueMatching))
        {
            // Beide Bilder passen zusammen => simulation das OpenCV einen Treffer findet!
            DataManager imageDBManager = new DataManager(this);
            newMaReId = -1;
            if (strPicPath != null && strPicName != null && strPicPathForImageView != null) {
                newPicId = imageDBManager.AddOrUpdatePictureImageData(strPicPath, strPicName, null, null, strPicPathForImageView, 0);
                System.err.println("DEBUG: MatchingActivity: btnTrueMatching: Neuer Eintrag zum Picture ist gespeichert!");
            }
            if (newPicId > 0 && strTempId != null) {
                // einfügen eines erfolgreiches Matchings
                // Es gibt bisher keine Ergebnisbild!
                // Ob das Matching erfolgreich ist oder nicht entscheidet ein Boolean Wert
                newMaReId = imageDBManager.AddOrUpdateMatchingData(newPicId, Integer.parseInt(strTempId), true, 0);
                System.err.println("DEBUG: MatchingActivity: btnTrueMatching: Neuer Eintrag zum erfolgreichen Matching Result ist gespeichert!");

                Toast.makeText(this, "Matching Result saved!.", Toast.LENGTH_LONG).show();
                iVShowPic.setImageBitmap(null);
                iVShowTemp.setImageBitmap(null);
                tVPicName.setText("");
                tVTempName.setText("");
            }
            if (newMaReId < 0)
            {
                Toast.makeText(this, "ERROR: MatchingActivity: btnTrueMatching:Es gab KEIN erfolgreichen Matching.", Toast.LENGTH_LONG).show();
                System.err.println("ERROR: MatchingActivity: btnTrueMatching: Es gab KEIN erfolgreichen Matching. Es gab keine Änderung in der Datenbank!");
            }
        }
        else if (view == findViewById(R.id.btnDoesNotMatch))
        {
            // Beide Bilder passen zusammen => simulation das OpenCV einen Treffer findet!
            DataManager imageDBManager = new DataManager(this);
            newMaReId = -1;
            if (strPicPath != null && strPicName != null && strPicPathForImageView != null) {
                newPicId = imageDBManager.AddOrUpdatePictureImageData(strPicPath, strPicName, null, null, strPicPathForImageView, 0);
                System.err.println("DEBUG: MatchingActivity: btnTrueMatching: Neuer Eintrag zum Picture ist gespeichert!");
            }
            if (newPicId > 0 && strTempId != null) {
                // einfügen eines erfolgreiches Matchings
                // Es gibt bisher keine Ergebnisbild!
                // Ob das Matching erfolgreich ist oder nicht entscheidet ein Boolean Wert
                newMaReId = imageDBManager.AddOrUpdateMatchingData(newPicId, Integer.parseInt(strTempId), false, 0);
                System.err.println("DEBUG: MatchingActivity: btnTrueMatching: Neuer Eintrag zum NICHT erfolgreichen Matching Result ist gespeichert!");

                Toast.makeText(this, "Miss Matching Result saved!.", Toast.LENGTH_LONG).show();
                iVShowPic.setImageBitmap(null);
                iVShowTemp.setImageBitmap(null);
                tVPicName.setText("");
                tVTempName.setText("");
            }
            if (newMaReId < 0)
            {
                Toast.makeText(this, "ERROR: MatchingActivity: btnTrueMatching: Es gab KEIN NICHT erfolgreichen Matching.", Toast.LENGTH_LONG).show();
                System.err.println("ERROR: MatchingActivity: btnTrueMatching: Es gab KEIN NICHT erfolgreichen Matching. Es gab keine Änderung in der Datenbank!");
            }
        }
        else if (view == findViewById(R.id.btnReturn))
        {
            finish();;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent itData) {
        super.onActivityResult(requestCode, resultCode, itData);

        // Die Ergebnisse der Aktion der Gallery werden hier verarbeitet
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && itData != null) {
            try {
                // Objekt auswerten
                Uri selectedImage = itData.getData();
                String[] strArrFilePathColumn = {MediaStore.Images.Media.DATA};
                Cursor curGal = getContentResolver().query(selectedImage, strArrFilePathColumn, null, null, null);
                curGal.moveToFirst();
                int intColumnIndex = curGal.getColumnIndex(strArrFilePathColumn[0]);
                strPicPathForImageView = curGal.getString(intColumnIndex);
                curGal.close();

                // Anzeigen des Bildes
                iVShowPic.setImageBitmap(BitmapFactory.decodeFile(strPicPathForImageView));

                // Angaben Pfad und Name
                String strPathFileName = strPicPathForImageView.toString();
                // aufteilen des Strings und ablegen auf globalend Variablen
                String[] strSplit = strPathFileName.split("/");
                strPicName = strSplit[strSplit.length - 1];
                strPicPath = strPathFileName.substring(0, strPathFileName.length() - strPicName.length());
                // Name des Bildes anzeigen
                tVPicName.setText(strPicName);
                Toast.makeText(this, "INFO: MatchingActivity: onActivityResult: Gallery: strPicPath: '" + strPicPathForImageView.toString() + "'", Toast.LENGTH_LONG).show();

            } catch (Exception strErr) {
                Toast.makeText(this,"ERROR: MatchingActivity: onActivityResult: Fehler beim Laden aus der Gallery:" + strErr.toString() , Toast.LENGTH_LONG).show();
                System.err.println("ERROR: MatchingActivity: onActivityResult: Fehler beim Laden aus der Gallery:" + strErr.toString());
            }
        }
        else if (requestCode == RESULT_TEMPLATE_INFO && resultCode == RESULT_OK && itData != null)
        {
            System.err.println("DEBUG: MatchingActivity: onActivityResult: RESULT_TEMPLATE_INFO : ..." +  itData.getExtras().toString());
            strTempId = itData.getStringExtra("strTempId");
            strTempName = itData.getStringExtra("strTempName");
            tVTempName.setText(strTempName);
            //strTempPath = itData.getStringExtra("strTempPath");
            System.err.println("DEBUG: MatchingActivity: strTempId   : " + strTempId );
            System.err.println("DEBUG: MatchingActivity: strTempName : " + strTempName );
            //System.err.println("DEBUG: MatchingActivity: strTempPath : " + strTempPath );

            Integer intTempId = Integer.parseInt(strTempId);
            objTEMPLATE objTEMPLATEFromDB = new objTEMPLATE();
            DataManager imageDBManager = new DataManager(this);
            objTEMPLATEFromDB = imageDBManager.getTemplateImageClassByIDFromDB(intTempId, objTEMPLATEFromDB);

            if (objTEMPLATEFromDB.t_cont.length > 0) {
                ByteArrayInputStream imageStream = new ByteArrayInputStream(objTEMPLATEFromDB.t_cont);
                iVShowTemp.setImageBitmap(BitmapFactory.decodeStream(imageStream));
            }
            else
            {
                System.err.println("ERROR: MatchingActivity: onActivityResult: -> objTEMPLATEFromDB hat kein BILD!");
            }
        }
    }

}

