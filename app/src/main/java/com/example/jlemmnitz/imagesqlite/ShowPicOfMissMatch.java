package com.example.jlemmnitz.imagesqlite;

import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.EditText;
import android.widget.Toast;

import java.io.ByteArrayInputStream;

public class ShowPicOfMissMatch extends ActionBarActivity implements android.view.View.OnClickListener {

    Button btnReturn;
    ImageView iVPic, iVTemp;
    EditText eTPicName, eTTempName;

    public Integer intMaReId = 0;
    public objMatchingResult objMR = new objMatchingResult();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_pic_of_miss_match);

        Button btnReturn = (Button) findViewById(R.id.btnReturn);
        ImageView iVPic = (ImageView) findViewById(R.id.iVPic);
        ImageView iVTemp = (ImageView) findViewById(R.id.iVTemp);
        EditText eTPicName = (EditText) findViewById(R.id.eTPicName);
        EditText eTTempName = (EditText) findViewById(R.id.eTTempName);

        btnReturn.setOnClickListener(this);

        // Übergabeparameter an die Activity auslesen
        Bundle bdlVar = getIntent().getExtras();
        if (bdlVar.size() > 0) {
            intMaReId = bdlVar.getInt("intMaReId");
            System.err.println("DEBUG: ShowPicOfMissMatch: intMaReId: " + intMaReId );
            // Lesen von Name+Pfad Pic & Name+Inhalt Temp,
        }

        // Lade Daten in Klasse aus der Datenbank
        DataManager imageDBManager = new DataManager(this);
        objMR = imageDBManager.getMatchingResult(intMaReId);

        if (objMR == null) {
            Toast.makeText(this, "ERROR: onCreate: objMR: is null!", Toast.LENGTH_LONG).show();
        }
        else {
            // Zeige den Namen von Picture und Template
            if (objMR.p_name != null) {
                eTPicName.setText(objMR.p_name);
            }
            if (objMR.t_name != null) {
                eTTempName.setText(objMR.t_name.toString());
            }

            // Lade das Picture von der SD Card
            if (objMR.p_path != null && objMR.p_name != null) {
                ByteArrayInputStream imageStream = new ByteArrayInputStream(imageDBManager.openImageFile(objMR.p_path + objMR.p_name));
                iVPic.setImageBitmap(BitmapFactory.decodeStream(imageStream));
            }

            // Lade das Template aus der Datenbank
            if (objMR.t_cont != null) {
                if (objMR.t_cont.length > 0) {
                    ByteArrayInputStream imageStream = new ByteArrayInputStream(objMR.t_cont);
                    iVTemp.setImageBitmap(BitmapFactory.decodeStream(imageStream));
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_pic_of_miss_match, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {
        if (view == findViewById(R.id.btnReturn)) {
            finish();
        }
    }

}
