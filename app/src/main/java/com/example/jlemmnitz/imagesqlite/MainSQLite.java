package com.example.jlemmnitz.imagesqlite;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
//import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import android.widget.Button;
import android.widget.ListView;
import java.util.ArrayList;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.util.ArrayList;

public class MainSQLite extends ActionBarActivity implements android.view.View.OnClickListener{

    // GUI Objects
    Button btnAddImage, btnUpdateList, btnCloseApp, btnMatchImage;
    ListView lVSQLiteContent;

    int RESULT_IMAGE_INFO_Add = 900;
    int RESULT_IMAGE_INFO_Update = 909;

    // Die Array Liste vom Type String wird in die ListView eingefügt
    ArrayList<String> alSQLLiteCont = new ArrayList<String>();
    // global Klasse
    //private DBAccess imageDBAccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_sqlite);

        // Die Komponenten der GUI auf die globabe Variable legen
        //  - weiterhin den OnClickListenHandler einfpgen
        btnAddImage = (Button) findViewById(R.id.btnAddImage);
        btnAddImage.setOnClickListener(this);
        btnUpdateList = (Button) findViewById(R.id.btnUpdateList);
        btnUpdateList.setOnClickListener(this);
        btnCloseApp = (Button) findViewById(R.id.btnCloseApp);
        btnCloseApp.setOnClickListener(this);
        btnMatchImage = (Button) findViewById(R.id.btnMatchImage);
        btnMatchImage.setOnClickListener(this);
        lVSQLiteContent = (ListView) findViewById(R.id.lVSQLiteContent);
        // Leeren Eintrag in die Liste einfügen: diese Activität, eine Simple einspaltige Liste, daie ArrayListe ist der Inhalt

        alSQLLiteCont.add("Empty");
        lVSQLiteContent.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, alSQLLiteCont));
        lVSQLiteContent.setOnItemClickListener(  new AdapterView.OnItemClickListener()
            {
                // der Listener bekommt nu einen onItemClick Handler!
                public void onItemClick(AdapterView<?> parent, View v, int position, long longObjectID)
                {
                    onItemClickAction(v, position, longObjectID);
                }
            }  );

        // Update der liste
        this.doBtnUpdate("onCreate");
        Toast.makeText(this, "MainSQLite: Start Done!", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_sqlite, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_menue_main_settings) {
            Toast.makeText(this, "DEBUG: MainSQLite: Settings Menue", Toast.LENGTH_LONG).show();
            return true;
        }

        if (id == R.id.action_menue_main_show_matches) {
            Toast.makeText(this, "DEBUG: MainSQLite: Settings Matching", Toast.LENGTH_LONG).show();
            Intent itShowListMissMatch = new Intent(this, ShowListMissMatch.class);
            itShowListMissMatch.putExtra("Matched", true);  // Es sollen alle Einträgen von erfolgreichen Matches gezeigt werden!
            itShowListMissMatch.putExtra("Title", "Show Matched");
            startActivity(itShowListMissMatch);
            return true;
        }

        if (id == R.id.action_menue_main_show_miss_matches) {
            Toast.makeText(this, "DEBUG: MainSQLite: Settings Miss-Machting", Toast.LENGTH_LONG).show();
            Intent itShowListMissMatch = new Intent(this, ShowListMissMatch.class);
            itShowListMissMatch.putExtra("Matched", false); // Es sollen alle Einträgen von NICHT erfolgreichen Matches gezeigt werden!
            itShowListMissMatch.putExtra("Title","Show Miss-Matched");
            startActivity(itShowListMissMatch);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view)
    {
        // Wer hat gedrueckt?
        if ( view == findViewById(R.id.btnAddImage) )
        {
            Toast.makeText(this, "DEBUG: MainSQLite: Add Image Button", Toast.LENGTH_LONG).show();
            // öffnen des zweitern Frames mit dem Bild!
            try {
                Intent itImageInfo = new Intent(this, ImageInfo.class);
                itImageInfo.putExtra("btnAddUpdateImage", "Add Image");
                startActivityForResult(itImageInfo, RESULT_IMAGE_INFO_Add);
            }
            catch (Exception strErr)
            {
                Toast.makeText(this, "ERROR: MainSQLite: Fehler beim Übertragen des Bildes an den nächsten Frame:" + strErr.toString(), Toast.LENGTH_LONG).show();
            }
        }
        else if ( view == findViewById(R.id.btnUpdateList) )
        {
            Toast.makeText(this, "DEBUG: MainSQLite: Update Image List Button", Toast.LENGTH_LONG).show();
            this.doBtnUpdate("onClick");
        }
        else if ( view == findViewById(R.id.btnMatchImage) )
        {
            Toast.makeText(this, "DEBUG: MainSQLite: Match Pic to Temp Button", Toast.LENGTH_LONG).show();
            // öffnen die Match Activity
            // hier sind Pic und Temp Verknüpfungen vorgegeben
            Intent itMatchingInfo = new Intent(this, MatchingActivity.class);
            startActivity(itMatchingInfo);
        }
        else if ( view == findViewById(R.id.btnCloseApp) )
        {
            Toast.makeText(this, "DEBUG: MainSQLite: Exit App Button", Toast.LENGTH_LONG).show();
            // Schliessen der App
            Toast.makeText(this, "MainSQLite: Good By", Toast.LENGTH_SHORT);

            // run copy...exprot an look inside
            //this.copyFile("/data/data/com.example.jlemmnitz.imagesqlite/databases/imagesql.db", "/data/data/com.example.jlemmnitz.imagesqlite/databases/imagesqlX.db");
            finish();
        }
        else
        {
            Toast.makeText(this, "DEBUG: MainSQLite: keine Action", Toast.LENGTH_LONG).show();
        }

    }

    /*
    public static boolean copyFile( String strSource, String strDest ) {
        try ( FileInputStream in = new FileInputStream( strSource );
              FileOutputStream out = new FileOutputStream( strDest ) ) {
            byte[] buffer = new byte[4096];
            int len;
            while ( (len = in.read( buffer )) > 0 )
                out.write( buffer, 0, len );
        }
        catch ( Exception e ) {
            System.err.println("ERROR: Main: copyFile: "  +e.toString());
            return false;
        }
        return true;
    }
    */

    public  void doBtnUpdate(String extraInfo) {
        // alle Einträge aus der DB lesen und die ID sowie den Namen anzeigen
        if (extraInfo== null) {extraInfo = ""; }
        System.err.println("DEBUG: MainSQLite: doBtnUpdate: .... " + extraInfo.toString());
        DataManager imageDBManager = new DataManager(this);
        ArrayList<String> alTempImageNameList = imageDBManager.getTemplateImageList();
        // wenn nicht leer!
        if (alTempImageNameList.isEmpty()) {
            lVSQLiteContent.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, alTempImageNameList));
            Toast.makeText(this, "WARNING: MainSQLite: No Template Image Names!", Toast.LENGTH_SHORT);
            System.err.println("WARNING: MainSQLite: No Template Image Names! ");
        } else {
            lVSQLiteContent.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, alTempImageNameList));
        }
        System.err.println("DEBUG: MainSQLite: doBtnUpdate: .... done " + extraInfo.toString());
    }


    public void onItemClickAction(View listeObject, int itemPosition, long longObjectID)
    {
        // Verarbeitung eines CLicks auf dem Eintrag einer Liste
        Toast.makeText(this, "DEBUG: MainSQLite: onItemCllick", Toast.LENGTH_LONG).show();
        System.err.println("DEBUG: MainSQLite: onItemCllick");
        // weder ie ID der View nch die ObjectID aus dem onItemClick Listene reichen zum Abgleich

        // Lesen den Eintrag und Splitte ImageIdName in ID und NAME
        String strImageIdName = lVSQLiteContent.getAdapter().getItem(itemPosition).toString();
        String[] strArr_Id_Name = strImageIdName.split(":");

        // aufrufen der anderen Activity
        if (strArr_Id_Name.length == 2) {

            // diverse parameter
            Intent itImageInfo = new Intent(getApplicationContext(), ImageInfo.class);
            itImageInfo.putExtra("strImageId", strArr_Id_Name[0].toString());
            itImageInfo.putExtra("strImageName", strArr_Id_Name[1].toString());
            itImageInfo.putExtra("strTableName", ImageDBModel.TABLE_Temp);
            //itImageInfo.putExtra("boolShowTempData","true"); // der String wird dann mit getBoolean in eine Boolean umewandelt
            //itImageInfo.putExtra("boolShowTempData", new Boolean(true));
            itImageInfo.putExtra("strShowTempData","EINS");
            itImageInfo.putExtra("btnAddUpdateImageTitle", "Update Image");
            //itImageInfo.putExtra("objTEMPLATEFromList", objTEMPLATEFromList);

            System.err.println("DEBUG: MainSQLite: go to <itImageInfo> with boolShowTempData = true");
            //startActivity(itImageInfo);
            startActivityForResult(itImageInfo, RESULT_IMAGE_INFO_Update);
        }
        else
        {
            Toast.makeText(this, "WARNING: MainSQLite: ListItem has no ':'", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onActivityResult(int requestedCode, int resultCode, Intent intActivityData)
    {
        // Zurück von der ImageInfo -> Template anlegen!
        if (requestedCode == RESULT_IMAGE_INFO_Add && intActivityData != null) //  resultCode == RESULT_OK &&
        {
            // Auswerden der übergeben Parameter
            System.err.println("INFO:  MainSQL: onActivityResult: Add Image finished: ReturnFrom: " + intActivityData.getStringExtra("ReturnFrom") + " ...");
            String strDoNext = intActivityData.getStringExtra("DoNext");
            System.err.println("DEBUG:  MainSQL: onActivityResult: DoNext: " + strDoNext.toString());
            this.doBtnUpdate("onActivityResult");
        }
        else if (requestedCode == RESULT_IMAGE_INFO_Update && intActivityData != null) //  resultCode == RESULT_OK &&
        {
            // Auswerden der übergeben Parameter
            System.err.println("INFO:  MainSQL: onActivityResult: Update Image finished: ReturnFrom: " + intActivityData.getStringExtra("ReturnFrom") + " ...");
            String strDoNext = intActivityData.getStringExtra("DoNext");
            System.err.println("DEBUG:  MainSQL: onActivityResult: DoNext: " + strDoNext.toString());
            this.doBtnUpdate("onActivityResult");
        }
        else
        {
            System.err.println("WARNING: MainSQL: onActivityResult: requestedCode=" + requestedCode + " resultCode=" + resultCode); // + " intActivityData=" + intActivityData.toString());
        }
    }
}
